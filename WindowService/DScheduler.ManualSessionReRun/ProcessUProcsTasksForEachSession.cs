﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using DScheduler.Common;
using DScheduler.DataAccess;
using DScheduler.Framework;
using DScheduler.Framework.Helpers;

namespace DScheduler.ManualSessionReRun
{
    class ProcessUProcsTasksForEachSession : EnumerateTask<IEnumerable<ScheduledSessionDetails>>
    {
        public string connectionString;
        public ProcessUProcsTasksForEachSession(string connectionString)
        {
            // TODO: Complete member initialization
            this.connectionString = connectionString;
        }

        #region Batch Framework overrides

        /// <summary>
        /// Override batch framework task cleanup method
        /// </summary>
        protected override void CleanUp()
        {
            base.CleanUp();
        }

        /// <summary>
        /// Override batch framework task initialize method
        /// </summary>
        protected override void Initialize()
        {
            base.Initialize();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        protected override void ProcessItem(IEnumerable<ScheduledSessionDetails> item)
        {
            if (item != null)
            {
                foreach (var processSessionRecords in item)
                {
                    /*
                     * We need to add our validations here for UProc perspective, whether we need to process this UProc or not
                     * Validations Cover Outage 
                     * Covers last Uproc/Session completed validations
                     * Covers Recurrence Validations
                    */
                    if (processSessionRecords != null)
                    {
                        string state = "Started";
                        UpdateJobMontior(processSessionRecords, false, state);
                        bool flag = ProcessJobs(processSessionRecords);
                        state = "Completed";
                        UpdateJobMontior(processSessionRecords, flag, state);
                    }
                }
            }
        }



        #endregion

        #region private methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dRow"></param>
        /// <returns></returns>
        private bool ProcessJobs(ScheduledSessionDetails dRow)
        {
            try
            {
                List<TUproc> fetchUProcRecords;
                TUproc records = new TUproc();
                bool error = false;
                if (dRow != null && dRow.UProcID != null && Convert.ToInt32(dRow.UProcID) > 0)
                {
                    int uprocId = Convert.ToInt32(dRow.UProcID);
                    using (DScheduleEntities dbConnection = new DScheduleEntities(connectionString))
                    {                        
                        fetchUProcRecords = (from abc in dbConnection.TUprocs
                                             where abc.UprocID == uprocId
                                             select abc as TUproc).ToList();

                        records = dbConnection.TUprocs.FirstOrDefault(x => x.UprocID == uprocId);
                    }
                    if (records != null)
                    {
                        using (Process pr = new Process())
                        {
                            pr.StartInfo = new ProcessStartInfo()
                            {
                                FileName = records.FolderPath + "\\" + records.UprocName,
                                RedirectStandardError = true,
                                RedirectStandardOutput = true,
                                WindowStyle = ProcessWindowStyle.Hidden,
                                UseShellExecute = false,
                                CreateNoWindow = true,
                                Arguments = "50"
                            };
                            StringBuilder errorBuilder = new StringBuilder();
                            pr.ErrorDataReceived += delegate (object sender, DataReceivedEventArgs e)
                            {
                                errorBuilder.Append(e.Data);
                            };

                            //call this before process start
                            pr.StartInfo.RedirectStandardError = true;
                            pr.Start();
                            //call this after process start
                            pr.BeginErrorReadLine();
                            string strLog;
                            while ((strLog = pr.StandardOutput.ReadLine()) != null)
                            {
                                Logger.WriteLog(strLog);
                            }
                            if (errorBuilder.Length > 0)
                            {
                                Logger.WriteLog(errorBuilder.ToString());
                                error = true;
                            }
                            pr.WaitForExit();
                        }
                    }
                    return error ? false : true;
                }
            }
            catch (Exception Ex)
            {
                Logger.WriteLog(Ex.Message);
            }
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dRow"></param>
        /// <param name="newStatus"></param>
        /// <param name="state"></param>
        private void UpdateJobMontior(ScheduledSessionDetails dRow, bool newStatus, string state)
        {
            DateTime currentDate = DateTime.Now;
            using (DScheduleEntities dbConnection = new DScheduleEntities(connectionString))
            {
                string sessionRunId = dRow.SessionRunID;
                int uprocId = Convert.ToInt32(dRow.UProcID);
                var records = (from monitor in dbConnection.TJobMonitors
                               where monitor.SessionRunID == sessionRunId
                               && monitor.UprocID == uprocId
                               orderby monitor.StartedDateTime descending
                               select monitor).FirstOrDefault();

                if (records != null)
                {
                    if (state.Equals("Completed"))
                    {
                        records.UProcStatus = newStatus ? "Completed" : "Failed";
                        records.CompletedDateTime = currentDate;
                    }
                    else
                    {
                        records.UProcStatus = "Started";
                        records.StartedDateTime = currentDate;
                    }
                    records.UpdatedBy = "admin2";
                    records.UpdatedDateTime = currentDate;
                    
                }
                dbConnection.SaveChanges();
            }
        }
        #endregion
    }
}


