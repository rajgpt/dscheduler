﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DScheduler.DataAccess
{
    //Partial class created to incorporate the connectionString explicitly passed from the front end
    public partial class DScheduleEntities : IDisposable
    {
        public DScheduleEntities(string connectionString)
            : base(connectionString)
        {
        }
       
    }
}
