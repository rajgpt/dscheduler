﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Messaging;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using DScheduler.BusinessServices;
using DScheduler.Common;
using DScheduler.PeriodicSessionRun;
using System.Runtime.InteropServices;

namespace ExecuteJobRulesUsingWS
{
    public partial class DSchedulerPeriodicService : ServiceBase
    {
        private System.Timers.Timer timer = null;
        private int dailySchedulerInterval;
        public int numberOfThreads { get; set; }

        public DSchedulerPeriodicService()
        {
            InitializeComponent();
            string serviceInterval = ConfigurationManager.AppSettings["SchedulerInterval"];
            dailySchedulerInterval = Convert.ToInt32(serviceInterval); //* 60 * 100 * 60; //converting number of hours into seconds
            numberOfThreads = Convert.ToInt32(ConfigurationManager.AppSettings["NumberOfThreads"].ToString());
        }

        protected override void OnStart(string[] args)
        {
            // eventLogger.WriteEntry("In OnStart");

            Logger.WriteLog("Periodic Service Execution Started");
            // TODO: Add code here to start your service.
            timer = new System.Timers.Timer();
            this.timer.Interval = dailySchedulerInterval;
            this.timer.Elapsed += timer_Elapsed;
            this.timer.Start();
        }

        protected override void OnStop()
        {
            // eventLogger.WriteEntry("In OnStop");
            Logger.WriteLog("Periodic Service Execution Stopped");
            this.timer.Enabled = false;
            // TODO: Add code here to perform any tear-down necessary to stop your service.
        }

        private void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            Logger.WriteLog("Timer event handler,ProcessPeriodicServiceInformation called" + DateTime.Now);
            ProcessPeriodicServiceInformation psi = new ProcessPeriodicServiceInformation();
            var conn = ConfigurationManager.ConnectionStrings[Constants.DBConnectionString].ConnectionString;
            psi.ExecutePeriodicJobs(conn, DateTime.Now, dailySchedulerInterval, numberOfThreads);
        }

        public enum ServiceState
        {
            SERVICE_STOPPED = 0x00000001,
            SERVICE_START_PENDING = 0x00000002,
            SERVICE_STOP_PENDING = 0x00000003,
            SERVICE_RUNNING = 0x00000004,
            SERVICE_CONTINUE_PENDING = 0x00000005,
            SERVICE_PAUSE_PENDING = 0x00000006,
            SERVICE_PAUSED = 0x00000007,
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct ServiceStatus
        {
            public long dwServiceType;
            public ServiceState dwCurrentState;
            public long dwControlsAccepted;
            public long dwWin32ExitCode;
            public long dwServiceSpecificExitCode;
            public long dwCheckPoint;
            public long dwWaitHint;
        };

    }
}
