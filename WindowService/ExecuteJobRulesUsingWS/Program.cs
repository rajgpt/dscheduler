﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Messaging;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using DScheduler.Common;
using DScheduler.PeriodicSessionRun;

namespace ExecuteJobRulesUsingWS
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //ServiceBase[] ServicesToRun;
            //ServicesToRun = new ServiceBase[]
            //{
            //    new ProcessPeriodicServiceInformation()
            //};
            //ServiceBase.Run(ServicesToRun);

            ProcessPeriodicServiceInformation psi = new ProcessPeriodicServiceInformation();
            var conn = System.Configuration.ConfigurationManager.ConnectionStrings[Constants.DBConnectionString].ConnectionString;
            string serviceInterval = ConfigurationManager.AppSettings["SchedulerInterval"];
            int numberOfThreads = Convert.ToInt32(ConfigurationManager.AppSettings["NumberOfThreads"].ToString());
            int schedulerInterval = Convert.ToInt32(serviceInterval);
            psi.ExecutePeriodicJobs(conn, DateTime.Now, schedulerInterval, numberOfThreads);
        }
    }
}
