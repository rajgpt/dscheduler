﻿

using DScheduler.Framework;
namespace DScheduler.Common
{
    public class ConvertDataRowToEntities : FilterDataTable<ScheduledSessionDetails, ScheduledSessionsConverter>
    {
        #region Batch Framework Overrides

        protected override void Initialize()
        {
            base.Initialize();
        }

        protected override void CleanUp()
        {
            base.CleanUp();
        }

        #endregion Batch Framework Overrides
    }

    public class ConvertPeriodicDataRowToEntities : FilterDataTable<ScheduledSessionDetails, ScheduledPeriodicSessionsConverter>
    {
        #region Batch Framework Overrides

        protected override void Initialize()
        {
            base.Initialize();
        }

        protected override void CleanUp()
        {
            base.CleanUp();
        }

        #endregion Batch Framework Overrides
    }

}