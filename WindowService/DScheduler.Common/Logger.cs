﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using DScheduler.Framework.Helpers;

namespace DScheduler.Common
{
    public class Logger
    {
        #region Logging
        public static void WriteLog(string message)
        {
            //StreamWriter sw = null;
            try
            {
                //sw = new StreamWriter(@"C:\Dev1\MSCoPSchedularService.log", true);
                //sw.WriteLine(DateTime.Now.ToString() + " : " + message);
                //sw.Flush();
                //sw.Close();
                var helper = new SqlClientHelper();
                helper.RunProcedure(Constants.SpSaveErrorLog, Constants.DefaultConnectionString, true,
                                           new KeyValuePair<string, object>(Constants.ExceptionMsg, message),
                                           new KeyValuePair<string, object>(Constants.ExceptionType, "ServiceTest"),
                                           new KeyValuePair<string, object>(Constants.ExceptionSource, "ServiceTest"),
                                           new KeyValuePair<string, object>(Constants.ExceptionURL, "null"),
                                           new KeyValuePair<string, object>(Constants.ErrorId, 0));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static void WriteLog(string message, string jobName)
        {
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(@"C:\Dev1\MSCoPSchedularService_" + jobName + ".log", true);
                sw.WriteLine(DateTime.Now.ToString() + " : " + message);
                sw.Flush();
                sw.Close();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
