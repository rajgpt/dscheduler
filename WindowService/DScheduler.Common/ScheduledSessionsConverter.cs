﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DScheduler.Common;
using DScheduler.Framework;

namespace DScheduler.Common
{
    public class ScheduledSessionsConverter : DataRowConverter<ScheduledSessionDetails>
    {
        /// <summary>
        /// Overrides the Assign function
        /// </summary>
        protected override void Assign()
        {
            SetField<string>(Constants.UProcIDs, (t, p) => t.UProcID = p);

            SetField<string>(Constants.SessionID, (t, p) => t.SessionID = p);

            SetField<string>(Constants.SessionRunId, (t, p) => t.SessionRunID = p);

            SetField<string>(Constants.JobAction, (t, p) => t.JobAction = p);
            // SetField<System.TimeSpan>(Constants.JobStartTime, (t, p) => t.JobStartTime = p);

        }
    }

    public class ScheduledPeriodicSessionsConverter : DataRowConverter<ScheduledSessionDetails>
    {
        /// <summary>
        /// Overrides the Assign function
        /// </summary>
        protected override void Assign()
        {
            SetField<string>(Constants.UProcIDs, (t, p) => t.UProcID = p);

            SetField<string>(Constants.SessionID, (t, p) => t.SessionID = p);

            SetField<System.TimeSpan>(Constants.JobStartTime, (t, p) => t.JobStartTime = p);
            
        }
    }
}