﻿using System;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using DScheduler.BusinessServices;
using System.Configuration;
using DScheduler.Common;
using DScheduler.PeriodicSessionRun;

namespace DScheduler.WindowsService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
            new DSchedulerDailyService()
            };
            ServiceBase.Run(ServicesToRun);

            //ProcessServiceInformation psi = new ProcessServiceInformation();
            //var conn = System.Configuration.ConfigurationManager.ConnectionStrings[Constants.DBConnectionString].ConnectionString;
            //string serviceInterval = ConfigurationManager.AppSettings["SchedulerInterval"];
            //int numberOfThreads = Convert.ToInt32(ConfigurationManager.AppSettings["NumberOfThreads"].ToString());
            //int schedulerInterval = Convert.ToInt32(serviceInterval);
            //psi.FetchJobRecords(conn, DateTime.Now.TimeOfDay, schedulerInterval, numberOfThreads);
            //ProcessPeriodicServiceInformation psi2 = new ProcessPeriodicServiceInformation();
            //psi2.ExecutePeriodicJobs(conn, DateTime.Now, schedulerInterval, numberOfThreads);

        }

    }
}

