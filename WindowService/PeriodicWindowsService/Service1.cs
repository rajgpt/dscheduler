﻿using DScheduler.BusinessServices;
using DScheduler.Common;
using DScheduler.PeriodicSessionRun;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace PeriodicWindowsService
{
    public partial class Service1 : ServiceBase
    {
        private System.Timers.Timer timer = null;
        private int schedulerInterval;
        private int numberOfThreads;
        public Service1()
        {
            InitializeComponent();
            string serviceInterval = ConfigurationManager.AppSettings["SchedulerInterval"];
            schedulerInterval = Convert.ToInt32(serviceInterval);
            numberOfThreads = Convert.ToInt32(ConfigurationManager.AppSettings["NumberOfThreads"].ToString());

        }

      
        protected override void OnStart(string[] args)
        {
            // eventLogger.WriteEntry("In OnStart");

            Logger.WriteLog("Service Execution Started");
            // TODO: Add code here to start your service.
            timer = new System.Timers.Timer();
            this.timer.Interval = schedulerInterval;
            this.timer.Elapsed += Timer_Elapsed;
            this.timer.Start();
        }

        private void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            Logger.WriteLog("Timer event handler,ProcessServiceInformation called" + DateTime.Now);
            ProcessPeriodicServiceInformation psi = new ProcessPeriodicServiceInformation();
            var conn = ConfigurationManager.ConnectionStrings[Constants.DBConnectionString].ConnectionString;
            psi.ExecutePeriodicJobs(conn, DateTime.Now, schedulerInterval, numberOfThreads);

        }

        protected override void OnStop()
        { 
            // eventLogger.WriteEntry("In OnStop");
            Logger.WriteLog("Service Execution Stopped");
            this.timer.Enabled = false;
            // TODO: Add code here to perform any tear-down necessary to stop your service.

        }
    }
}
