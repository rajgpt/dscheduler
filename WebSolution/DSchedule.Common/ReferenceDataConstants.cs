﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSchedule.Common
{
    public class ReferenceDataConstants
    {
        public class ActivityType
        {
            public const int UprocRefId = 9;
            public const int SessionRefId = 10;
        }

        public class RecPattType
        {
            public const int DailyId = 11;
            public const int WeeklyId = 12;
            public const int MonthlyId = 13;
            public const int YearlyId = 14;
        }
    }
}
