﻿using DSchedule.Contracts.Models;
using DSchedule.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSchedule.BusinessComponent
{
    public class UserBusinessComponent
    {
        DSchedulerEntities context = new DSchedulerEntities();


        public bool ValidateUser(string userName, string password)
        {
            bool isValid = false;
            using (var db = new DSchedulerEntities())
            {
                var user = db.TLogins.FirstOrDefault(u => u.UserName == userName
                                        && u.Password == password && u.IsActive == "1");
                if (user != null)
                {
                    isValid = true;
                }
            }
            return isValid;
        }



        public List<UserProfile> getUserDetails()
        {
            using (var db = new DSchedulerEntities())
            {
                var userDBList = db.TLogins == null ? null : db.TLogins.Select(x => new UserProfile
                {
                    LoginID=x.LoginID,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    Email = x.Email,
                    IsActive = x.IsActive,
                    UserName = x.UserName,
                    AccountTypeId = x.AccountTypeID,
                    AccountType = x.TRef.RefDescription,
                    Organization = x.Organization
                }).ToList();

                return userDBList;
            }
        }



        private IEnumerable<ReferenceData> GetAccountTypes()
        {
            //var accountTypes = context.AccountTypes
            //            .OrderBy(x => x.AccountTypeName)
            //            .ToList()
            //            .Select(x => new SelectListItem
            //            {
            //                Text = x.AccountTypeName,
            //                Value = x.AccountTypeId.ToString()
            //            });

            //return new SelectList(accountTypes, "Value", "Text");
            return null;
        }

        public void CreateNewUser(UserProfile userProfile, string userName)
        {
            try
            {

                bool insert = false;
                TLogin dbUserProfile = null;
                if (userProfile.LoginID != 0)
                {
                    dbUserProfile = context.TLogins.FirstOrDefault(x => x.LoginID == userProfile.LoginID);
                }
                if (dbUserProfile == null)
                {
                    dbUserProfile = new TLogin();
                    insert = true;
                }

                if (userProfile != null)
                {
                    dbUserProfile.AccountTypeID = userProfile.AccountTypeId;
                    dbUserProfile.CreatedDateTime = DateTime.Now;
                    dbUserProfile.CreatedBy = userName;
                    dbUserProfile.Email = userProfile.Email;
                    dbUserProfile.FirstName = userProfile.FirstName;
                    dbUserProfile.IsActive = userProfile.IsActive;
                    dbUserProfile.LastName = userProfile.LastName;
                    dbUserProfile.Organization = userProfile.Organization;
                    dbUserProfile.Password = userProfile.Password;
                    dbUserProfile.UpdatedDateTime = DateTime.Now;
                    dbUserProfile.UpdatedBy = userName;
                    dbUserProfile.UserName = userProfile.UserName;
                    dbUserProfile.TRef = null;
                }
                if (insert)
                {
                    context.TLogins.Add(dbUserProfile);
                }

                context.SaveChanges();
            }
            catch (Exception ex)
            {

            }

            }
    }
}
