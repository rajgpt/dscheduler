﻿using DSchedule.BusinessComponent;
using DSchedule.Common;
using DSchedule.Contracts;
using DSchedule.Contracts.Models;
using DSchedule.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Environment = DSchedule.Contracts.Models.Environment;
using System.Globalization;
using System.Data.Entity;

namespace DSchedule.BusinessComponent
{
    public class DScheduleBusinessComponent
    {
        DSchedulerEntities context = new DSchedulerEntities();

        public object ViewBag { get; private set; }

        #region Nodes

        public NodesModel GetNodes()
        {
            NodesModel node = new NodesModel() { NodesList = new List<Node>() };

            var tempNodes = (from TNode in context.TNodes
                             select TNode).ToList();

            if (tempNodes.Any())
            {
                tempNodes.ForEach(x => node.NodesList.Add(new Node
                {
                    NodeID = x.NodeID,
                    CreatedBy = x.CreatedBy,
                    CreatedDateTime = x.CreatedDateTime,
                    NodeName = x.NodeName,
                    NodePath = x.NodePath,
                    UpdatedBy = x.UpdatedBy,
                    UpdatedDateTime = x.UpdatedDateTime
                }));
            }

            return node;
        }

        public bool SaveNode(Node node)
        {
            if (node != null)
            {
                if (node.NodeID == 0)
                {
                    // Use Unique Node name
                    if (context.TNodes.Any(x => x.NodeName == node.NodeName))
                    {
                        return false;
                    }

                    context.TNodes.Add(new TNode
                    {
                        NodeName = node.NodeName,
                        NodePath = node.NodePath,
                        CreatedBy = GlobalFunctions.GetLoggedInUser(),
                        CreatedDateTime = GlobalFunctions.GetDate(),
                        UpdatedBy = GlobalFunctions.GetLoggedInUser(),
                        UpdatedDateTime = GlobalFunctions.GetDate()
                    });
                }
                else
                {
                    TNode entity = context.TNodes.FirstOrDefault(x => x.NodeID == node.NodeID);
                    if (entity != null)
                    {
                        entity.NodeName = node.NodeName;
                        entity.NodePath = node.NodePath;
                        entity.UpdatedBy = GlobalFunctions.GetLoggedInUser();
                        entity.UpdatedDateTime = GlobalFunctions.GetDate();
                    }
                }

                context.SaveChanges();
            }

            return true;
        }

        #endregion

        #region Environment

        public bool SaveEnvironment(Environment environment)
        {
            if (environment != null)
            {
                if (environment.EnvironmentID == 0)
                {
                    // Use Unique Environment name
                    if (context.TEnvironments.Any(x => x.EnvironmentName == environment.EnvironmentName))
                    {
                        return false;
                    }

                    context.TEnvironments.Add(new TEnvironment
                    {
                        CreatedBy = GlobalFunctions.GetLoggedInUser(),
                        CreatedDateTime = GlobalFunctions.GetDate(),
                        EnvironmentID = environment.EnvironmentID,
                        EnvironmentName = environment.EnvironmentName,
                        EnvironmentPath = environment.EnvironmentPath,
                        NodeID = environment.NodeID,
                        UpdatedBy = GlobalFunctions.GetLoggedInUser(),
                        UpdatedDateTime = GlobalFunctions.GetDate()
                    });
                }
                else
                {
                    TEnvironment entity = context.TEnvironments.FirstOrDefault(x => x.EnvironmentID == environment.EnvironmentID);

                    entity.UpdatedBy = GlobalFunctions.GetLoggedInUser();
                    entity.UpdatedDateTime = GlobalFunctions.GetDate();
                    entity.NodeID = environment.NodeID;
                    entity.EnvironmentPath = environment.EnvironmentPath;
                    entity.EnvironmentName = environment.EnvironmentName;
                }

                context.SaveChanges();
            }

            return true;
        }

        public EnvironmentModel GetEnvironments()
        {
            EnvironmentModel model = new EnvironmentModel();
            List<Environment> environments = new List<Environment>();
            List<Node> nodes = new List<Node>();

            var tempEnvironments = (from env in context.TEnvironments
                                    let nod = env.TNode
                                    select new
                                    {
                                        env.EnvironmentPath,
                                        env.EnvironmentName,
                                        env.EnvironmentID,
                                        env.CreatedDateTime,
                                        env.NodeID,
                                        nod.NodeName
                                    }).ToList();

            var tempNodes = (from node in context.TNodes
                             select new { node.NodeID, node.NodeName }).ToList();

            if (tempEnvironments.Any())
            {
                tempEnvironments.ForEach(x => environments.Add(new Environment
                {
                    EnvironmentID = x.EnvironmentID,
                    EnvironmentName = x.EnvironmentName,
                    CreatedDateTime = x.CreatedDateTime,
                    EnvironmentPath = x.EnvironmentPath,
                    NodeID = x.NodeID,
                    NodeName = x.NodeName
                }));
            }

            if (tempNodes.Any())
            {
                tempNodes.ForEach(x => nodes.Add(new Node { NodeID = x.NodeID, NodeName = x.NodeName }));
            }

            model.NodesList = nodes;
            model.EnvironmentList = environments;
            return model;
        }
        public List<Node> GetNodeList()
        {
            List<Node> nodes = new List<Node>();
            var tempNodes = (from node in context.TNodes
                             select new { node.NodeID, node.NodeName }).ToList();
            if (tempNodes.Any())
            {
                tempNodes.ForEach(x => nodes.Add(new Node { NodeID = x.NodeID, NodeName = x.NodeName }));
            }
            return nodes;
        }
        #endregion

        #region Uprocs

        public UprocsModel GetUprocs()
        {
            UprocsModel uproc = new UprocsModel();

            var tempUprocs = (from TUproc in context.TUprocs
                              select TUproc).ToList();

            if (tempUprocs.Any())
            {
                tempUprocs.ForEach(x => uproc.UprocsList.Add(new Uprocs
                {
                    UprocID = x.UprocID,
                    UprocName = x.UprocName,
                    JobTypeID = x.JobTypeID,
                    EnvironmentID = x.EnvironmentID,
                    AccountTypeID = x.AccountTypeID,
                    EnvironmentName = x.EnvironmentName,
                    FolderPath = x.FolderPath,
                    Command = x.Command
                }));
            }
            return uproc;
        }

        public bool SaveUprocDetails(UprocsModel uprocs)
        {
            bool success = false;
            if (uprocs.UprocID == 0)
            {
                TUproc obj = new TUproc();
                obj.UprocID = uprocs.UprocID;
                obj.UprocName = uprocs.UprocName;
                obj.JobTypeID = uprocs.JobTypeID;
                obj.AccountTypeID = uprocs.AccountTypeID;
                obj.EnvironmentID = uprocs.EnvironmentID;
                obj.EnvironmentName = uprocs.EnvironmentName;
                obj.FolderPath = uprocs.FolderPath;
                obj.Command = uprocs.Command;
                obj.IsActive = uprocs.IsActive;
                obj.CreatedBy = GlobalFunctions.GetLoggedInUser();
                obj.CreatedDateTime = GlobalFunctions.GetDate();
                obj.UpdatedBy = GlobalFunctions.GetLoggedInUser();
                obj.UpdatedDateTime = GlobalFunctions.GetDate();
                context.TUprocs.Add(obj);
            }
            else
            {
                TUproc obj = context.TUprocs.FirstOrDefault(x => x.UprocID == uprocs.UprocID);
                obj.UprocName = uprocs.UprocName;
                obj.JobTypeID = uprocs.JobTypeID;
                obj.AccountTypeID = uprocs.AccountTypeID;
                obj.EnvironmentID = uprocs.EnvironmentID;
                obj.EnvironmentName = uprocs.EnvironmentName;
                obj.FolderPath = uprocs.FolderPath;
                obj.Command = uprocs.Command;
                obj.IsActive = uprocs.IsActive;
                obj.UpdatedBy = GlobalFunctions.GetLoggedInUser();
                obj.UpdatedDateTime = GlobalFunctions.GetDate();
            }
            context.SaveChanges();
            success = true;
            return success;
        }

        public List<ReferenceData> GetUprocsReferenceData()
        {
            List<ReferenceData> listRefData = new List<ReferenceData>();
            List<string> listUprocsRefData = new List<string>();
            listUprocsRefData.Add("AccountTypeID");
            listUprocsRefData.Add("JobTypeID");
            var tempRefdata = (from TRef in context.TRefs
                               where listUprocsRefData.Contains(TRef.RefName)
                               select TRef).ToList();
            //Adding Account and Job Type Refdata to list of ReferenceData
            if (tempRefdata.Any())
            {
                tempRefdata.ForEach(x => listRefData.Add(new ReferenceData
                {
                    RefId = x.RefID,
                    RefName = x.RefName,
                    RefValue = x.RefValue,
                    RefDescription = x.RefDescription,
                    IsActive = x.IsActive,
                    IsVisible = x.IsVisible
                }));
            }
            //Fetching Environment Ref Data
            var envRef = (from env in context.TEnvironments
                          select env).ToList();
            if (envRef.Any())
            {
                envRef.ForEach(x => listRefData.Add(new ReferenceData
                {
                    RefId = x.EnvironmentID,
                    RefName = "EnvironmentTypeID",
                    RefValue = x.EnvironmentName,
                    RefDescription = x.EnvironmentName,
                    IsActive = "Y",
                    IsVisible = "Y"
                }));
            }
            return listRefData;
        }

        #endregion



        #region Sessions

        public SessionsModel GetSessions()
        {
            SessionsModel session = new SessionsModel();

            var tempSessions = (from sessionItem in context.TSessions
                                join dependencyItem in context.TDependencies
                                on sessionItem.SessionID equals dependencyItem.SessionID
                                select new
                                {
                                    SessionId = sessionItem.SessionID,
                                    EnvironmentId = sessionItem.EnvironmentID,
                                    AccountTypeId = sessionItem.AccountTypeID,
                                    Session = sessionItem.SessionName,
                                    EnvironmentName = sessionItem.EnvironmentName,
                                    UprocId = dependencyItem.UprocID,
                                }).ToList();
            //var tempTDependencies = (from dependentItem in context.TDependencies
            //                         select dependentItem).ToList();
            if (tempSessions.Any())
            {
                //tempSessions.ForEach(x => session.SessionsList.Add(new Sessions
                //{
                //    SessionId = x.SessionID,
                //    EnvironmentID = x.EnvironmentID,
                //    AccountTypeID = x.AccountTypeID,
                //    Session = x.SessionName,
                //    Environment = x.EnvironmentName,

                //}
                //));
                foreach (var item in tempSessions)
                {
                    session.SessionsList.Add(new Sessions
                    {
                        SessionId = item.SessionId,
                        EnvironmentID = item.EnvironmentId,
                        AccountTypeID = item.AccountTypeId,
                        Session = item.Session,
                        Environment = item.EnvironmentName,
                        UprocId = item.UprocId.HasValue ? item.UprocId.Value : 0,
                    }
                    );
                }
            }
            return session;
        }

        public bool SaveSessions(SessionsModel request)
        {
            int? maxSequenceNumber = 0;
            bool success = false;

            if (request.command == "Add")
            {
                maxSequenceNumber = context.TDependencies.Where(x => x.SessionID == request.DependentSessionId).Max(y => y.SequenceNumber);
                TDependency dep = new TDependency();
                dep.DependentID = -1;
                dep.SequenceNumber = maxSequenceNumber + 1;
                dep.SessionID = request.DependentSessionId;
                dep.UprocID = request.DependentUprocId;
                dep.CreatedBy = GlobalFunctions.GetLoggedInUser();
                dep.CreatedDateTime = GlobalFunctions.GetDate();
                dep.UpdatedBy = GlobalFunctions.GetLoggedInUser();
                dep.UpdatedDateTime = GlobalFunctions.GetDate();
                context.TDependencies.Add(dep);
            }
            else if (request.SessionId == 0 && request.command == "Save")
            {
                //Adding record in the TSession Table
                TSession obj = new TSession();
                obj.SessionID = request.SessionId;
                obj.SessionName = request.SessionName;
                obj.EnvironmentID = request.EnvironmentID;
                obj.EnvironmentName = request.EnvironmentName;
                obj.AccountTypeID = request.AccountTypeID;
                obj.CreatedBy = GlobalFunctions.GetLoggedInUser();
                obj.CreatedDateTime = GlobalFunctions.GetDate();
                obj.UpdatedBy = GlobalFunctions.GetLoggedInUser();
                obj.UpdatedDateTime = GlobalFunctions.GetDate();
                context.TSessions.Add(obj);

                //Adding record in the TDependencies table
                TDependency depObj = new TDependency();
                depObj.DependentID = 0;
                depObj.SessionID = request.SessionId;
                depObj.UprocID = request.UprocID;
                depObj.SequenceNumber = 1; // Default seq no value for new session
                depObj.CreatedBy = GlobalFunctions.GetLoggedInUser();
                depObj.CreatedDateTime = GlobalFunctions.GetDate();
                depObj.UpdatedBy = GlobalFunctions.GetLoggedInUser();
                depObj.UpdatedDateTime = GlobalFunctions.GetDate();
                context.TDependencies.Add(depObj);
                //code to create dependent session
                if (request.DependentSessionId != 0 && request.DependentUprocId != 0)
                {
                    int maxSeq = 0;
                    maxSeq = context.TDependencies.Where(x => x.SessionID == request.DependentSessionId).Max(y => Convert.ToInt32(y.SequenceNumber));
                    TDependency dep = new TDependency();
                    dep.DependentID = -1;
                    dep.SessionID = request.DependentSessionId;
                    dep.UprocID = request.DependentUprocId;
                    dep.SequenceNumber = maxSeq + 1;
                    dep.CreatedBy = GlobalFunctions.GetLoggedInUser();
                    dep.CreatedDateTime = GlobalFunctions.GetDate();
                    dep.UpdatedBy = GlobalFunctions.GetLoggedInUser();
                    dep.UpdatedDateTime = GlobalFunctions.GetDate();
                    context.TDependencies.Add(dep);
                }
            }
            else
            {
                TSession obj = context.TSessions.FirstOrDefault(x => x.SessionID == request.SessionId);
                obj.SessionID = request.SessionId;
                obj.SessionName = request.SessionName;
                obj.EnvironmentID = request.EnvironmentID;
                obj.EnvironmentName = request.EnvironmentName;
                obj.AccountTypeID = request.AccountTypeID;
                obj.UpdatedBy = GlobalFunctions.GetLoggedInUser();
                obj.UpdatedDateTime = GlobalFunctions.GetDate();
            }

            context.SaveChanges();
            success = true;
            return success;
        }
        public List<ReferenceData> GetSessionsReferenceData()
        {
            List<ReferenceData> listRefData = new List<ReferenceData>();
            List<string> listSessionsRefData = new List<string>();
            listSessionsRefData.Add("AccountTypeID");
            var tempRefdata = (from TRef in context.TRefs
                               where listSessionsRefData.Contains(TRef.RefName)
                               select TRef).ToList();
            //Adding Account and Job Type Refdata to list of ReferenceData
            if (tempRefdata.Any())
            {
                tempRefdata.ForEach(x => listRefData.Add(new ReferenceData
                {
                    RefId = x.RefID,
                    RefName = x.RefName,
                    RefValue = x.RefValue,
                    RefDescription = x.RefDescription,
                    IsActive = x.IsActive,
                    IsVisible = x.IsVisible
                }));
            }
            //Fetching Environment Ref Data
            var envRef = (from env in context.TEnvironments
                          select env).ToList();
            if (envRef.Any())
            {
                envRef.ForEach(x => listRefData.Add(new ReferenceData
                {
                    RefId = x.EnvironmentID,
                    RefName = "EnvironmentTypeID",
                    RefValue = x.EnvironmentName,
                    RefDescription = x.EnvironmentName,
                    IsActive = "Y",
                    IsVisible = "Y"
                }));
            }
            //Fetching Uproc Reference Data
            var uprocRef = (from uproc in context.TUprocs
                            select uproc).ToList();
            if (uprocRef.Any())
            {
                uprocRef.ForEach(x => listRefData.Add(new ReferenceData
                {
                    RefId = x.UprocID,
                    RefName = "UprocsTypeID",
                    RefValue = x.UprocName,
                    RefDescription = x.UprocName,
                    IsActive = "Y",
                    IsVisible = "Y"
                }));
            }
            //Fetching Sessions reference data
            var sessionRef = (from session in context.TDependencies
                              select session.TSession).Distinct().ToList();
            if (sessionRef.Any())
            {
                sessionRef.ForEach(x => listRefData.Add(new ReferenceData
                {
                    RefId = x.SessionID,
                    RefName = "SessionsID",
                    RefValue = x.SessionName,
                    RefDescription = x.SessionName,
                    IsActive = "Y",
                    IsVisible = "Y"
                }));
            }

            //Fetching Dependent Uprocs reference data
            var dependentUprocsRef = (from dUprocs in context.TDependencies
                                      select dUprocs.TUproc).Distinct().ToList();
            if (dependentUprocsRef.Any())
            {
                dependentUprocsRef.ForEach(x => listRefData.Add(new ReferenceData
                {
                    RefId = x.UprocID,
                    RefName = "DependentUprocsID",
                    RefValue = x.UprocName,
                    RefDescription = x.UprocName,
                    IsActive = "Y",
                    IsVisible = "Y"
                }));
            }
            return listRefData;
        }
        #endregion

        public List<TRef> GetReferenceData(string referenceTable)
        {
            List<TRef> tRefList = new List<TRef>();
            TRef tRef;
            var tempUprocs = (from accountTypes in context.TRefs
                              where accountTypes.RefName == referenceTable
                              select accountTypes).ToList();

            foreach (TRef myRef in tempUprocs)
            {
                tRef = new TRef();
                tRef.RefName = myRef.RefName;
                tRef.RefValue = myRef.RefValue;
                tRef.RefDescription = myRef.RefDescription;
                tRef.RefID = myRef.RefID;
                tRefList.Add(tRef);
            }
            return tRefList;
        }

        public dynamic GetRefTableValueDetails(List<TRef> accountTypes)
        {

            var accountDetails = accountTypes.OrderBy(x => x.RefDescription)
                        .ToList()
                        .Select(x => new SelectListItem
                        {
                            Text = x.RefDescription,
                            Value = x.RefValue.ToString()
                        });

            return new SelectList(accountDetails, "Value", "Text");
        }

        public List<ReferenceData> GetReferenceDataNew(string referenceTable)
        {
            var refDataList = (from refs in context.TRefs
                               where refs.RefName == referenceTable
                               select refs).ToList();
            var referenceList = new List<ReferenceData>();

            refDataList.ForEach(x => referenceList.Add(new ReferenceData
            {
                RefId = x.RefID,
                RefName = x.RefName,
                RefValue = x.RefValue,
                RefDescription = x.RefDescription,
                IsActive = x.IsActive,
                IsVisible = x.IsVisible
            }));
            return referenceList;
        }
        internal object getJson(JobMonitor jobMonitorModel)
        {
            var jsonObj =
                  (from obj in context.TJobMonitors
                   where obj.SessionRunID == jobMonitorModel.SessionRunID
                   // && obj.NodeID == jobMonitorModel.NodeID
                   && obj.UprocID == jobMonitorModel.UprocID
                   select new { UprocID = jobMonitorModel.UprocID, UProcStatus = jobMonitorModel.UProcStatus, SessionRunId = jobMonitorModel.SessionRunID }).ToList().FirstOrDefault();

            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(jsonObj);

        }

        public List<TRef> GetAccountTypes()
        {
            List<TRef> tRefList = new List<TRef>();
            TRef tRef;
            var tempUprocs = from accountTypes in context.TRefs
                             where accountTypes.RefName == "AccountTypeID"
                             select accountTypes;

            foreach (TRef myRef in tempUprocs)
            {
                tRef = new TRef();
                tRef.RefName = myRef.RefName;
                tRef.RefValue = myRef.RefValue;
                tRef.RefDescription = myRef.RefDescription;
                tRef.RefID = myRef.RefID;
                tRefList.Add(tRef);
            }
            return tRefList;
        }

        public NewUpdateJob GetJob()
        {
            NewUpdateJob newupdateJob = new NewUpdateJob();

            newupdateJob.NewUpdateJobs = new List<NewUpdateJobModel>();

            newupdateJob.UprocsList = (from uproc in context.TUprocs
                                       select new Uprocs
                                       {
                                           UprocID = uproc.UprocID,
                                           UprocName = uproc.UprocName
                                       }).ToList();

            newupdateJob.SessionsList = (from session in context.TSessions
                                         select new Sessions
                                         {
                                             SessionId = session.SessionID,
                                             Session = session.SessionName
                                         }).ToList();

            newupdateJob.EnvironmentList = (from env in context.TEnvironments
                                            select new Environment
                                            {
                                                EnvironmentID = env.EnvironmentID,
                                                EnvironmentName = env.EnvironmentName
                                            }).ToList();

            newupdateJob.NewUpdateJobs = (from job in context.TJobRules
                                          select new NewUpdateJobModel
                                          {
                                              UprocId = job.UprocID.HasValue ? job.UprocID.Value : 0,
                                              UprocName = job.TUproc.UprocName,
                                              EnvironmentId = job.TUproc.EnvironmentID,
                                              EnvironmentName = job.TUproc.EnvironmentName,
                                              NodeId = job.TUproc.TEnvironment.NodeID,
                                              NodeName = job.TUproc.TEnvironment.TNode.NodeName,
                                              SessionId = job.SessionID.HasValue ? job.SessionID.Value : 0,
                                              SessionName = job.TSession.SessionName,
                                              //ScheduledStartDate=job.JobStartTime,
                                              CreateDate = job.CreatedDateTime,
                                              CreatedBy = job.CreatedBy
                                          }).ToList();

            return newupdateJob;
        }

        public bool SaveJob(NewUpdateJob model)
        {
            TJobRule jobRule = null;
            if (model.JobRuleId != 0)
            {
                jobRule = context.TJobRules.Where(x => x.JobRuleID == model.JobRuleId).FirstOrDefault();
            }

            bool add = false;
            if (jobRule == null)
            {
                add = true;
                jobRule = new TJobRule();
                jobRule.CreatedBy = GlobalFunctions.GetLoggedInUser();
                jobRule.CreatedDateTime = GlobalFunctions.GetDate();
            }

            jobRule.UpdatedBy = GlobalFunctions.GetLoggedInUser();
            jobRule.UpdatedDateTime = GlobalFunctions.GetDate();
            jobRule.UprocID = model.UprocId;
            jobRule.JobStartTime = model.JobStartTime;
            jobRule.SessionID = model.SessionId;
            jobRule.JobStartTime = model.JobStartTime;

            if (add)
            {
                context.TJobRules.Add(jobRule);
            }

            context.SaveChanges();

            return true;
        }

        public List<Outage> getOutageDetailsbyJobRuleID(int jobRuleID)
        {
            var outageList = context.TOutageDetails.Where(o => o.JobRuleID == jobRuleID);
            List<Outage> outage = new List<Outage>();

            foreach (var o in outageList)
            {
                Outage tOutage = new Outage();
                tOutage.CreatedBy = GlobalFunctions.GetLoggedInUser();
                tOutage.CreatedDateTime = GlobalFunctions.GetDate();
                tOutage.UpdatedBy = GlobalFunctions.GetLoggedInUser();
                tOutage.UpdatedDateTime = GlobalFunctions.GetDate();
                tOutage.JobRuleId = o.JobRuleID;
                tOutage.OutageName = o.OutageName;
                tOutage.OutageStartDate = o.OutageStartDate.Value.Date.ToString("d");
                tOutage.OutageStartTime = o.OutageStartTime;
                tOutage.OutageEndTime = o.OutageEndTime;
                outage.Add(tOutage);
            }
            return outage;
        }

        public JobRulesModel GetJobRules()
        {
            JobRulesModel jobRulesModel = new JobRulesModel();

            var jobRules = (from jobRule in context.TJobRules
                            select jobRule).ToList();

            //var jobRules = (from jobRule in context.TJobRules
            //                group jobRule by jobRule.SessionID into a
            //                select a.OrderByDescending(x => x.UpdatedDateTime)).ToList();

            jobRulesModel.JobRulesList = new List<JobRules>();
            foreach (var jobRule in jobRules)
            {
                try
                {
                    var outageList = context.TOutageDetails.Where(o => o.JobRuleID == jobRule.JobRuleID);
                    List<Outage> outage = new List<Outage>();

                    foreach (var o in outageList)
                    {
                        Outage tOutage = new Outage();
                        tOutage.CreatedBy = GlobalFunctions.GetLoggedInUser();
                        tOutage.CreatedDateTime = GlobalFunctions.GetDate();
                        tOutage.UpdatedBy = GlobalFunctions.GetLoggedInUser();
                        tOutage.UpdatedDateTime = GlobalFunctions.GetDate();
                        tOutage.JobRuleId = o.JobRuleID;
                        tOutage.OutageName = o.OutageName;
                        tOutage.OutageStartDate = o.OutageStartDate.Value.ToString();
                        tOutage.OutageStartTime = o.OutageStartTime;
                        tOutage.OutageEndTime = o.OutageEndTime;
                        outage.Add(tOutage);
                    }
                    JobRules jr = new JobRules()
                    {
                        jobRuleID = jobRule.JobRuleID,
                        UprocId = jobRule.UprocID.HasValue ? jobRule.UprocID.Value : 0,
                        UprocName = jobRule.TUproc != null ? jobRule.TUproc.UprocName : null,
                        EnvironmentId = jobRule.TUproc != null ? jobRule.TUproc.EnvironmentID : jobRule.TSession != null ? jobRule.TSession.EnvironmentID : 0,
                        NodeId = jobRule.TUproc != null ? jobRule.TUproc.TEnvironment.NodeID : jobRule.TSession != null ? jobRule.TSession.TEnvironment.NodeID : 0,
                        SessionId = jobRule.SessionID.HasValue ? jobRule.SessionID.Value : 0,
                        Environment = jobRule.TUproc != null ? jobRule.TUproc.EnvironmentName : jobRule.TSession != null ? jobRule.TSession.EnvironmentName : null,
                        SessionName = jobRule.TSession != null ? jobRule.TSession.SessionName : null,
                        Node = jobRule.TUproc != null ? jobRule.TUproc.TEnvironment.TNode.NodeName : jobRule.TSession != null ? jobRule.TSession.TEnvironment.TNode.NodeName : null,
                        CreatedBy = jobRule.CreatedBy,
                        CreatedDate = jobRule.CreatedDateTime,
                        UpdatedBy = jobRule.UpdatedBy,
                        UpdatedDate = jobRule.UpdatedDateTime,
                        ScheduledDateTime = jobRule.JobStartTime,
                        IsRecurrencePresent = jobRule.IsRecurrencePresent != null ? jobRule.IsRecurrencePresent.Value : false,
                        Outage = outage

                    };
                    if (Convert.ToBoolean(jr.IsRecurrencePresent))
                    {
                        jr.RecurrencePattern = context.TRecurrencePatterns.Where(rec => rec.JobRuleID == jobRule.JobRuleID).Select(s => s.RecPattTypeID.Value).FirstOrDefault();
                        jr.RecurrenceStartDate = context.TRecurrenceRanges.Where(rec => rec.jobruleID == jobRule.JobRuleID).Select(s => s.StartDateTime.Value).FirstOrDefault();
                        jr.RecurrenceEndDate = context.TRecurrenceRanges.Where(rec => rec.jobruleID == jobRule.JobRuleID).Select(s => s.EndDateTime.Value).FirstOrDefault();
                        int recurrencePatternID = context.TRecurrencePatterns.Where(rec => rec.JobRuleID == jobRule.JobRuleID).Select(s => s.RecurrencePatternID).FirstOrDefault();

                        if (jr.RecurrencePattern == context.TRefs.Where(s => s.RefName.ToLower() == "RecPattTypeID" && s.RefDescription.ToLower() == "weekly").Select(m => m.RefID).FirstOrDefault())
                        {
                            var weeklyDetails = (from reccurDtl in context.TRecurrencePatternDetails
                                                 where reccurDtl.RecurrencePatternId == recurrencePatternID
                                                 select reccurDtl).ToList();
                            foreach (var recur in weeklyDetails)
                            {
                                if (recur.DayOfWeek == context.TRefs.Where(s => s.RefName.ToLower() == "weekdaytypeid" && s.RefDescription.ToLower() == "monday").Select(m => m.RefDescription).FirstOrDefault().ToString())
                                {
                                    jr.IsMonday = true;
                                }
                                if (recur.DayOfWeek == context.TRefs.Where(s => s.RefName.ToLower() == "weekdaytypeid" && s.RefDescription.ToLower() == "tuesday").Select(m => m.RefDescription).FirstOrDefault().ToString())
                                {
                                    jr.IsTuesday = true;
                                }
                                if (recur.DayOfWeek == context.TRefs.Where(s => s.RefName.ToLower() == "weekdaytypeid" && s.RefDescription.ToLower() == "wednesday").Select(m => m.RefDescription).FirstOrDefault().ToString())
                                {
                                    jr.IsWednesday = true;
                                }
                                if (recur.DayOfWeek == context.TRefs.Where(s => s.RefName.ToLower() == "weekdaytypeid" && s.RefDescription.ToLower() == "thursday").Select(m => m.RefDescription).FirstOrDefault().ToString())
                                {
                                    jr.IsThursday = true;
                                }
                                if (recur.DayOfWeek == context.TRefs.Where(s => s.RefName.ToLower() == "weekdaytypeid" && s.RefDescription.ToLower() == "friday").Select(m => m.RefDescription).FirstOrDefault().ToString())
                                {
                                    jr.IsFriday = true;
                                }
                                if (recur.DayOfWeek == context.TRefs.Where(s => s.RefName.ToLower() == "weekdaytypeid" && s.RefDescription.ToLower() == "saturday").Select(m => m.RefDescription).FirstOrDefault().ToString())
                                {
                                    jr.IsSaturday = true;
                                }
                                if (recur.DayOfWeek == context.TRefs.Where(s => s.RefName.ToLower() == "weekdaytypeid" && s.RefDescription.ToLower() == "sunday").Select(m => m.RefDescription).FirstOrDefault().ToString())
                                {
                                    jr.IsSunday = true;
                                }
                            }

                        }
                        else if (jr.RecurrencePattern == context.TRefs.Where(s => s.RefName.ToLower() == "RecPattTypeID" && s.RefDescription.ToLower() == "monthly").Select(m => m.RefID).FirstOrDefault())
                        {
                            jr.RMDay = (from reccurDtl in context.TRecurrencePatternDetails
                                        where reccurDtl.RecurrencePatternId == recurrencePatternID
                                        select reccurDtl.DayOfMonth).FirstOrDefault().ToString();
                        }
                        else if (jr.RecurrencePattern == context.TRefs.Where(s => s.RefName.ToLower() == "RecPattTypeID" && s.RefDescription.ToLower() == "yearly").Select(m => m.RefID).FirstOrDefault())
                        {
                            jr.RYDay = (from reccurDtl in context.TRecurrencePatternDetails
                                        where reccurDtl.RecurrencePatternId == recurrencePatternID
                                        select reccurDtl.DayOfYear).FirstOrDefault().ToString();
                        }
                    }
                    jobRulesModel.JobRulesList.Add(jr);
                }
                catch (Exception ex)
                {

                    throw;
                }
            }

            return jobRulesModel;
        }

        public bool checkJobRule(JobRulesModel jobRuleModel)
        {
            var reccurencepattern = Convert.ToInt32(jobRuleModel.RecurrencePattern);
            var recordExists = (from jobrules in context.TJobRules
                                join recurrence in context.TRecurrencePatterns on jobrules.JobRuleID equals recurrence.JobRuleID
                                where jobrules.SessionID == jobRuleModel.UprocOrSessionId && recurrence.RecPattTypeID.Value == reccurencepattern
                                select jobrules.SessionID
                                        ).FirstOrDefault();
            return recordExists.HasValue;
        }

        public bool SaveJobRule(JobRulesModel jobRuleModel)
        {
            TJobRule tJobRule;
            if (jobRuleModel.JobRuleId == 0)
            {
                tJobRule = new TJobRule()
                {

                    CreatedBy = GlobalFunctions.GetLoggedInUser(),
                    CreatedDateTime = GlobalFunctions.GetDate(),
                    UpdatedBy = GlobalFunctions.GetLoggedInUser(),
                    UpdatedDateTime = GlobalFunctions.GetDate(),
                    JobStartTime = TimeSpan.Parse(Convert.ToDateTime(jobRuleModel.JobStartDate).ToString("HH:mm")),
                    ActivityTypeID = jobRuleModel.ActivityTypeID != 10 ? 10 : jobRuleModel.ActivityTypeID,
                    IsRecurrencePresent = jobRuleModel.IsRecurrence
                };
                if (jobRuleModel.ActivityTypeID == Common.ReferenceDataConstants.ActivityType.UprocRefId)
                {
                    tJobRule.UprocID = jobRuleModel.UprocOrSessionId;
                }
                else
                {

                    tJobRule.SessionID = jobRuleModel.UprocOrSessionId;
                }
                context.TJobRules.Add(tJobRule);
                context.SaveChanges();
                var jobRuleID = tJobRule.JobRuleID;


                if (jobRuleModel.Outage != null && jobRuleModel.Outage.Any())
                {
                    var outagedetele = context.TOutageDetails.Where(x => x.JobRuleID == jobRuleID);
                    if (outagedetele.Count() > 0)
                    {
                        context.TOutageDetails.RemoveRange(outagedetele);
                    }
                    context.SaveChanges();

                    foreach (var outage in jobRuleModel.Outage)
                    {
                        TOutageDetail tOutage = new TOutageDetail();
                        tOutage.CreatedBy = GlobalFunctions.GetLoggedInUser();
                        tOutage.CreatedDateTime = GlobalFunctions.GetDate();
                        tOutage.UpdatedBy = GlobalFunctions.GetLoggedInUser();
                        tOutage.UpdatedDateTime = GlobalFunctions.GetDate();
                        tOutage.JobRuleID = jobRuleID;
                        tOutage.OutageName = outage.OutageName;
                        tOutage.OutageStartDate = Convert.ToDateTime(outage.OutageStartDate);
                        tOutage.OutageStartTime = outage.OutageStartTime;
                        tOutage.OutageEndTime = outage.OutageEndTime;
                        tJobRule.TOutageDetails.Add(tOutage);
                    }
                }

                if (jobRuleModel.IsRecurrence)
                {
                    TRecurrenceRange tRecurRange = context.TRecurrenceRanges.Where(m => m.jobruleID == jobRuleID).FirstOrDefault();
                    if (tRecurRange == null)
                    {
                        tRecurRange = new TRecurrenceRange();
                    }
                    tRecurRange.CreatedBy = GlobalFunctions.GetLoggedInUser();
                    tRecurRange.UpdatedBy = GlobalFunctions.GetLoggedInUser(); ;
                    tRecurRange.UpdatedDateTime = GlobalFunctions.GetDate();
                    tRecurRange.CreatedDateTime = GlobalFunctions.GetDate();
                    tRecurRange.StartDateTime = jobRuleModel.RecurrenceStartDate;
                    tRecurRange.EndDateTime = jobRuleModel.RecurrenceEndDate;
                    tRecurRange.RecRangeTypeID = context.TRefs.Where(m => m.RefName.ToLower() == "recrangetypeid" && m.RefValue.ToLower() == "end by").Select(s => s.RefID).FirstOrDefault();
                    tRecurRange.jobruleID = jobRuleID;
                    context.TRecurrenceRanges.Add(tRecurRange);

                    // Add recurrence
                    TRecurrencePattern tRecurrence = new TRecurrencePattern();
                    int recurrencePatternTypeId = 0;
                    if (int.TryParse(jobRuleModel.RecurrencePattern, out recurrencePatternTypeId))
                    {
                        switch (recurrencePatternTypeId)
                        {
                            case ReferenceDataConstants.RecPattType.DailyId:
                                break;

                            case ReferenceDataConstants.RecPattType.MonthlyId:

                                break;

                            case ReferenceDataConstants.RecPattType.WeeklyId:

                                break;

                            case ReferenceDataConstants.RecPattType.YearlyId:
                                break;
                        }
                    }
                    tRecurrence.JobRuleID = jobRuleID;
                    tRecurrence.CreatedDateTime = GlobalFunctions.GetDate();
                    tRecurrence.UpdatedDateTime = GlobalFunctions.GetDate();
                    tRecurrence.UpdatedBy = GlobalFunctions.GetLoggedInUser();
                    tRecurrence.CreatedBy = GlobalFunctions.GetLoggedInUser();
                    tRecurrence.IsActiveRecurrence = jobRuleModel.IsRecurrence;
                    tRecurrence.RecPattTypeID = recurrencePatternTypeId;
                    tJobRule.TRecurrencePatterns.Add(tRecurrence);
                    context.SaveChanges();
                    var reccurenceID = tRecurrence.RecurrencePatternID;
                    TRecurrencePatternDetail recurDetail = new TRecurrencePatternDetail();
                    switch (recurrencePatternTypeId)
                    {
                        case ReferenceDataConstants.RecPattType.DailyId:
                            break;

                        case ReferenceDataConstants.RecPattType.MonthlyId:
                            recurDetail.RecurrencePatternId = reccurenceID;
                            recurDetail.CreateDate = GlobalFunctions.GetDate();
                            recurDetail.UpdatedDate = GlobalFunctions.GetDate();
                            recurDetail.UpdatedBy = GlobalFunctions.GetLoggedInUser();
                            recurDetail.CreatedBy = GlobalFunctions.GetLoggedInUser();
                            recurDetail.DayOfMonth = Convert.ToInt32(jobRuleModel.RMDay);
                            context.TRecurrencePatternDetails.Add(recurDetail);
                            break;

                        case ReferenceDataConstants.RecPattType.WeeklyId:
                            List<string> days = new List<string>();
                            if (jobRuleModel.IsMonday)
                            {
                                days.Add(context.TRefs.Where(s => s.RefName.ToLower() == "weekdaytypeid" && s.RefDescription.ToLower() == "monday").Select(m => m.RefDescription).FirstOrDefault().ToString());
                            }
                            if (jobRuleModel.IsTuesday)
                            {
                                days.Add(context.TRefs.Where(s => s.RefName.ToLower() == "weekdaytypeid" && s.RefDescription.ToLower() == "tuesday").Select(m => m.RefDescription).FirstOrDefault().ToString());
                            }
                            if (jobRuleModel.IsWednesday)
                            {
                                days.Add(context.TRefs.Where(s => s.RefName.ToLower() == "weekdaytypeid" && s.RefDescription.ToLower() == "wednesday").Select(m => m.RefDescription).FirstOrDefault().ToString());
                            }
                            if (jobRuleModel.IsThursday)
                            {
                                days.Add(context.TRefs.Where(s => s.RefName.ToLower() == "weekdaytypeid" && s.RefDescription.ToLower() == "thursday").Select(m => m.RefDescription).FirstOrDefault().ToString());
                            }
                            if (jobRuleModel.IsFriday)
                            {
                                days.Add(context.TRefs.Where(s => s.RefName.ToLower() == "weekdaytypeid" && s.RefDescription.ToLower() == "friday").Select(m => m.RefDescription).FirstOrDefault().ToString());
                            }
                            if (jobRuleModel.IsSaturday)
                            {
                                days.Add(context.TRefs.Where(s => s.RefName.ToLower() == "weekdaytypeid" && s.RefDescription.ToLower() == "saturday").Select(m => m.RefDescription).FirstOrDefault().ToString());
                            }
                            if (jobRuleModel.IsSunday)
                            {
                                days.Add(context.TRefs.Where(s => s.RefName.ToLower() == "weekdaytypeid" && s.RefDescription.ToLower() == "sunday").Select(m => m.RefDescription).FirstOrDefault().ToString());
                            }
                            foreach (string str in days)
                            {
                                recurDetail.RecurrencePatternId = reccurenceID;
                                recurDetail.CreateDate = GlobalFunctions.GetDate();
                                recurDetail.UpdatedDate = GlobalFunctions.GetDate();
                                recurDetail.UpdatedBy = GlobalFunctions.GetLoggedInUser();
                                recurDetail.CreatedBy = GlobalFunctions.GetLoggedInUser();
                                recurDetail.DayOfWeek = str;
                                context.TRecurrencePatternDetails.Add(recurDetail);
                            }
                            break;

                        case ReferenceDataConstants.RecPattType.YearlyId:
                            recurDetail.RecurrencePatternId = reccurenceID;
                            recurDetail.CreateDate = GlobalFunctions.GetDate();
                            recurDetail.UpdatedDate = GlobalFunctions.GetDate();
                            recurDetail.UpdatedBy = GlobalFunctions.GetLoggedInUser();
                            recurDetail.CreatedBy = GlobalFunctions.GetLoggedInUser();
                            recurDetail.DayOfYear = Convert.ToDateTime(jobRuleModel.RYDay);
                            context.TRecurrencePatternDetails.Add(recurDetail);
                            break;
                    }
                }

            }
            else
            {
                tJobRule = context.TJobRules.FirstOrDefault(x => x.JobRuleID == jobRuleModel.JobRuleId);
                if (tJobRule != null)
                {
                    tJobRule.UpdatedBy = GlobalFunctions.GetLoggedInUser();
                    tJobRule.UpdatedDateTime = GlobalFunctions.GetDate();
                    tJobRule.JobStartTime = TimeSpan.Parse(Convert.ToDateTime(jobRuleModel.JobStartDate).ToString("HH:mm"));
                    tJobRule.ActivityTypeID = jobRuleModel.ActivityTypeID;
                    tJobRule.IsRecurrencePresent = jobRuleModel.IsRecurrence;

                    if (jobRuleModel.UprocOrSessionId == ReferenceDataConstants.ActivityType.UprocRefId)
                    {
                        tJobRule.UprocID = jobRuleModel.UprocOrSessionId;
                    }
                    else
                    {
                        tJobRule.SessionID = jobRuleModel.UprocOrSessionId;
                    }



                    if (jobRuleModel.Outage != null && jobRuleModel.Outage.Any())
                    {
                        var outagedetele = context.TOutageDetails.Where(x => x.JobRuleID == tJobRule.JobRuleID);
                        if (outagedetele.Count() > 0)
                        {
                            context.TOutageDetails.RemoveRange(outagedetele);
                        }
                        context.SaveChanges();

                        foreach (var outage in jobRuleModel.Outage)
                        {
                            TOutageDetail tOutage = new TOutageDetail();
                            tOutage.CreatedBy = GlobalFunctions.GetLoggedInUser();
                            tOutage.CreatedDateTime = GlobalFunctions.GetDate();
                            tOutage.UpdatedBy = GlobalFunctions.GetLoggedInUser();
                            tOutage.UpdatedDateTime = GlobalFunctions.GetDate();
                            tOutage.JobRuleID = tJobRule.JobRuleID;
                            tOutage.OutageName = outage.OutageName;
                            tOutage.OutageStartDate = Convert.ToDateTime(outage.OutageStartDate);
                            tOutage.OutageStartTime = outage.OutageStartTime;
                            tOutage.OutageEndTime = outage.OutageEndTime;
                            tJobRule.TOutageDetails.Add(tOutage);
                        }
                    }
                    if (jobRuleModel.IsRecurrence)
                    {

                        TRecurrenceRange tRecurRange = context.TRecurrenceRanges.Where(m => m.jobruleID == tJobRule.JobRuleID).FirstOrDefault();
                        if (tRecurRange == null)
                        {
                            tRecurRange = new TRecurrenceRange();
                        }
                        tRecurRange.CreatedBy = GlobalFunctions.GetLoggedInUser();
                        tRecurRange.UpdatedBy = GlobalFunctions.GetLoggedInUser();
                        tRecurRange.UpdatedDateTime = GlobalFunctions.GetDate();
                        tRecurRange.CreatedDateTime = GlobalFunctions.GetDate();
                        tRecurRange.StartDateTime = jobRuleModel.RecurrenceStartDate;
                        tRecurRange.EndDateTime = jobRuleModel.RecurrenceEndDate;
                        tRecurRange.RecRangeTypeID = context.TRefs.Where(m => m.RefName.ToLower() == "recrangetypeid" && m.RefValue.ToLower() == "end by").Select(s => s.RefID).FirstOrDefault();
                        tRecurRange.jobruleID = tJobRule.JobRuleID;
                        if (tRecurRange.RecurrenceRangeID == 0)
                        { context.TRecurrenceRanges.Add(tRecurRange); }


                        // Add recurrence
                        TRecurrencePattern tRecurrence = context.TRecurrencePatterns.FirstOrDefault(x => x.JobRuleID == jobRuleModel.JobRuleId);
                        if (tRecurrence == null)
                        {
                            tRecurrence = new TRecurrencePattern();
                        }
                        int recurrencePatternTypeId = 0;
                        if (int.TryParse(jobRuleModel.RecurrencePattern, out recurrencePatternTypeId))
                        {
                            switch (recurrencePatternTypeId)
                            {
                                case ReferenceDataConstants.RecPattType.DailyId:
                                    break;

                                case ReferenceDataConstants.RecPattType.MonthlyId:

                                    break;

                                case ReferenceDataConstants.RecPattType.WeeklyId:

                                    break;

                                case ReferenceDataConstants.RecPattType.YearlyId:
                                    break;
                            }
                        }
                        tRecurrence.CreatedDateTime = GlobalFunctions.GetDate();
                        tRecurrence.UpdatedDateTime = GlobalFunctions.GetDate();
                        tRecurrence.UpdatedBy = GlobalFunctions.GetLoggedInUser();
                        tRecurrence.CreatedBy = GlobalFunctions.GetLoggedInUser();
                        tRecurrence.IsActiveRecurrence = jobRuleModel.IsRecurrence;
                        tRecurrence.RecPattTypeID = recurrencePatternTypeId;
                        tJobRule.TRecurrencePatterns.Add(tRecurrence);
                        var recurdtldelete = context.TRecurrencePatternDetails.Where(x => x.RecurrencePatternId == tRecurrence.RecurrencePatternID);
                        if (recurdtldelete.Count() > 0)
                        {
                            context.TRecurrencePatternDetails.RemoveRange(recurdtldelete);
                        }
                        context.SaveChanges();
                        var reccurenceID = tRecurrence.RecurrencePatternID;
                        TRecurrencePatternDetail recurDetail = new TRecurrencePatternDetail();
                        switch (recurrencePatternTypeId)
                        {
                            case ReferenceDataConstants.RecPattType.DailyId:
                                break;

                            case ReferenceDataConstants.RecPattType.MonthlyId:
                                recurDetail.RecurrencePatternId = reccurenceID;
                                recurDetail.CreateDate = GlobalFunctions.GetDate();
                                recurDetail.UpdatedDate = GlobalFunctions.GetDate();
                                recurDetail.UpdatedBy = GlobalFunctions.GetLoggedInUser();
                                recurDetail.CreatedBy = GlobalFunctions.GetLoggedInUser();
                                recurDetail.DayOfMonth = Convert.ToInt32(jobRuleModel.RMDay);
                                context.TRecurrencePatternDetails.Add(recurDetail);
                                break;

                            case ReferenceDataConstants.RecPattType.WeeklyId:
                                List<string> days = new List<string>();
                                if (jobRuleModel.IsMonday)
                                {
                                    days.Add(context.TRefs.Where(s => s.RefName.ToLower() == "weekdaytypeid" && s.RefDescription.ToLower() == "monday").Select(m => m.RefDescription).FirstOrDefault().ToString());
                                }
                                if (jobRuleModel.IsTuesday)
                                {
                                    days.Add(context.TRefs.Where(s => s.RefName.ToLower() == "weekdaytypeid" && s.RefDescription.ToLower() == "tuesday").Select(m => m.RefDescription).FirstOrDefault().ToString());
                                }
                                if (jobRuleModel.IsWednesday)
                                {
                                    days.Add(context.TRefs.Where(s => s.RefName.ToLower() == "weekdaytypeid" && s.RefDescription.ToLower() == "wednesday").Select(m => m.RefDescription).FirstOrDefault().ToString());
                                }
                                if (jobRuleModel.IsThursday)
                                {
                                    days.Add(context.TRefs.Where(s => s.RefName.ToLower() == "weekdaytypeid" && s.RefDescription.ToLower() == "thursday").Select(m => m.RefDescription).FirstOrDefault().ToString());
                                }
                                if (jobRuleModel.IsFriday)
                                {
                                    days.Add(context.TRefs.Where(s => s.RefName.ToLower() == "weekdaytypeid" && s.RefDescription.ToLower() == "friday").Select(m => m.RefDescription).FirstOrDefault().ToString());
                                }
                                if (jobRuleModel.IsSaturday)
                                {
                                    days.Add(context.TRefs.Where(s => s.RefName.ToLower() == "weekdaytypeid" && s.RefDescription.ToLower() == "saturday").Select(m => m.RefDescription).FirstOrDefault().ToString());
                                }
                                if (jobRuleModel.IsSunday)
                                {
                                    days.Add(context.TRefs.Where(s => s.RefName.ToLower() == "weekdaytypeid" && s.RefDescription.ToLower() == "sunday").Select(m => m.RefDescription).FirstOrDefault().ToString());
                                }
                                List<TRecurrencePatternDetail> weeklist = new List<TRecurrencePatternDetail>();
                                foreach (string str in days)
                                {
                                    TRecurrencePatternDetail detail = new TRecurrencePatternDetail();

                                    detail.RecurrencePatternId = reccurenceID;
                                    detail.CreateDate = GlobalFunctions.GetDate();
                                    detail.UpdatedDate = GlobalFunctions.GetDate();
                                    detail.UpdatedBy = GlobalFunctions.GetLoggedInUser();
                                    detail.CreatedBy = GlobalFunctions.GetLoggedInUser();
                                    detail.DayOfWeek = str;
                                    weeklist.Add(detail);

                                }
                                context.TRecurrencePatternDetails.AddRange(weeklist);
                                break;

                            case ReferenceDataConstants.RecPattType.YearlyId:
                                recurDetail.RecurrencePatternId = reccurenceID;
                                recurDetail.CreateDate = GlobalFunctions.GetDate();
                                recurDetail.UpdatedDate = GlobalFunctions.GetDate();
                                recurDetail.UpdatedBy = GlobalFunctions.GetLoggedInUser();
                                recurDetail.CreatedBy = GlobalFunctions.GetLoggedInUser();
                                recurDetail.DayOfYear = Convert.ToDateTime(jobRuleModel.RYDay);
                                context.TRecurrencePatternDetails.Add(recurDetail);
                                break;
                        }
                    }
                    else
                    {
                        TRecurrencePattern tRecurrencedelete = context.TRecurrencePatterns.FirstOrDefault(x => x.JobRuleID == jobRuleModel.JobRuleId);
                        if (tRecurrencedelete != null)
                        {
                            context.TRecurrencePatterns.Remove(tRecurrencedelete);
                            var recurdtldelete = context.TRecurrencePatternDetails.Where(x => x.RecurrencePatternId == tRecurrencedelete.RecurrencePatternID);
                            if (recurdtldelete.Count() > 0)
                            {
                                context.TRecurrencePatternDetails.RemoveRange(recurdtldelete);
                            }
                        }
                    }
                }
            }

            context.SaveChanges();
            return true;
        }

        #region Reports
        public List<ReferenceData> GetReportsReferenceData()
        {
            List<ReferenceData> listRefData = new List<ReferenceData>();
            List<string> listReportsRefData = new List<string>();
            //Fetching Node Ref Data
            var nodeRef = (from node in context.TNodes
                           select node).ToList();
            if (nodeRef.Any())
            {
                nodeRef.ForEach(x => listRefData.Add(new ReferenceData
                {
                    RefId = x.NodeID,
                    RefName = "Nodes",
                    RefValue = x.NodeName,
                    RefDescription = x.NodeName,
                    IsActive = "Y",
                    IsVisible = "Y"
                }));

                listRefData.Add(new ReferenceData
                {
                    RefId = 0,
                    RefName = "Nodes",
                    RefValue = "",
                    RefDescription = "",
                    IsActive = "Y",
                    IsVisible = "Y"
                });
            }
            //Fetching Environment Ref Data
            var envRef = (from env in context.TEnvironments
                          select env).ToList();
            if (envRef.Any())
            {
                envRef.ForEach(x => listRefData.Add(new ReferenceData
                {
                    RefId = x.EnvironmentID,
                    RefName = "Environments",
                    RefValue = x.EnvironmentName,
                    RefDescription = x.EnvironmentName,
                    IsActive = "Y",
                    IsVisible = "Y"
                }));
                listRefData.Add(new ReferenceData
                {
                    RefId = 0,
                    RefName = "Environments",
                    RefValue = "",
                    RefDescription = "",
                    IsActive = "Y",
                    IsVisible = "Y"
                });
            }
            ////Fetching Uproc Reference Data
            //var uprocRef = (from uproc in context.TUprocs
            //                select uproc).ToList();
            //if (uprocRef.Any())
            //{
            //    uprocRef.ForEach(x => listRefData.Add(new ReferenceData
            //    {
            //        RefId = x.UprocID,
            //        RefName = "Uprocs",
            //        RefValue = x.UprocName,
            //        RefDescription = x.UprocName,
            //        IsActive = "Y",
            //        IsVisible = "Y"
            //    }));
            //    listRefData.Add(new ReferenceData
            //    {
            //        RefId = 0,
            //        RefName = "Uprocs",
            //        RefValue = "",
            //        RefDescription = "",
            //        IsActive = "Y",
            //        IsVisible = "Y"
            //    });
            //}
            //Fetching Sessions reference data
            var sessionRef = (from session in context.TSessions
                              select session).Distinct().ToList();
            if (sessionRef.Any())
            {
                sessionRef.ForEach(x => listRefData.Add(new ReferenceData
                {
                    RefId = x.SessionID,
                    RefName = "Sessions",
                    RefValue = x.SessionName,
                    RefDescription = x.SessionName,
                    IsActive = "Y",
                    IsVisible = "Y"
                }));
                listRefData.Add(new ReferenceData
                {
                    RefId = 0,
                    RefName = "Sessions",
                    RefValue = "",
                    RefDescription = "",
                    IsActive = "Y",
                    IsVisible = "Y"
                });
            }
            //Adding Template Ref Data
            listRefData.Add(new ReferenceData
            {
                RefId = 1,
                RefName = "Templates",
                RefValue = "Excel",
                RefDescription = "Excel",
                IsActive = "Y",
                IsVisible = "Y"
            });
            listRefData.Add(new ReferenceData
            {
                RefId = 2,
                RefName = "Templates",
                RefValue = "PDF",
                RefDescription = "PDF",
                IsActive = "Y",
                IsVisible = "Y"
            });

            //Adding Status Ref Data
            listRefData.Add(new ReferenceData
            {
                RefId = 0,
                RefName = "Status",
                RefValue = "",
                RefDescription = "",
                IsActive = "Y",
                IsVisible = "Y"
            });

            listRefData.Add(new ReferenceData
            {
                RefId = 1,
                RefName = "Status",
                RefValue = "Completed",
                RefDescription = "Completed",
                IsActive = "Y",
                IsVisible = "Y"
            });
            listRefData.Add(new ReferenceData
            {
                RefId = 2,
                RefName = "Status",
                RefValue = "Failed",
                RefDescription = "Failed",
                IsActive = "Y",
                IsVisible = "Y"
            });
            return listRefData;
        }

        public List<ReportsData> GenerateReportsData(ReportsModel model)
        {
            List<ReferenceData> refData = new List<ReferenceData>();
            refData = GetReportsReferenceData();
            List<ReportsData> list = new List<ReportsData>();
            IQueryable<TJobMonitor> query = context.TJobMonitors;
            if (model.NodeId != 0)
            {
                query = query.Where(x => x.NodeID == model.NodeId);
            }
            if (model.EnvironmentId != 0)
            {
                query = query.Where(x => x.EnvironmentID == model.EnvironmentId);
            }
            if (model.SessionId != 0)
            {
                query = query.Where(x => x.SessionID == model.SessionId);
            }
            if (!string.IsNullOrEmpty(model.Status))
            {
                query = query.Where(x => x.UProcStatus == model.Status);
            }
            if (model.UprocId != 0)
            {
                query = query.Where(x => x.UprocID == model.UprocId);
            }
            if (model.StartDate.HasValue)
            {
                query = query.Where(x => x.StartedDateTime >= model.StartDate.Value);
            }
            if (model.EndDate.HasValue)
            {
                query = query.Where(x => x.CompletedDateTime <= model.EndDate.Value);
            }
            var data = (from report in query
                        select new
                        {
                            JobId = report.JobID,
                            NodeId = report.NodeID,
                            EnvironmentId = report.EnvironmentID,
                            SessionId = report.SessionID,
                            UprocId = report.UprocID,
                            StartTime = report.StartedDateTime,
                            EndTime = report.CompletedDateTime,
                            Status = report.UProcStatus,
                        }).ToList();
            foreach (var item in data)
            {
                ReportsData obj = new ReportsData();
                obj.JobId = item.JobId;
                obj.NodeId = item.NodeId;
                obj.EnvironmentId = item.EnvironmentId;
                obj.SessionId = item.SessionId;
                obj.UprocId = item.UprocId;
                obj.NodeName = refData.Where(x => x.RefName == "Nodes" && x.RefId == item.NodeId).FirstOrDefault().RefValue;
                obj.EnvironmentName = refData.Where(x => x.RefName == "Environments" && x.RefId == item.EnvironmentId).FirstOrDefault().RefValue;
                obj.SessionName = refData.Where(x => x.RefName == "Sessions" && x.RefId == item.SessionId).FirstOrDefault().RefValue;
                //obj.UprocName = refData.Where(x => x.RefName == "Uprocs" && x.RefId == item.UprocId).FirstOrDefault().RefValue;
                obj.StartTime = item.StartTime;
                obj.EndTime = item.EndTime;
                obj.ElapsedTime = GetTimeElapsed(item.StartTime, item.EndTime);
                obj.Status = item.Status;
                list.Add(obj);
            }
            return list;
        }

        public TimeSpan GetTimeElapsed(DateTime? start, DateTime? end)
        {
            TimeSpan t = TimeSpan.Zero;
            if (end.HasValue && start.HasValue)
            {
                t = end.Value - start.Value;
            }
            return t;
        }
        #endregion

    }
}
