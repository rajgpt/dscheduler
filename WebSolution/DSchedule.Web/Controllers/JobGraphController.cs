﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DSchedule.BusinessComponent;
using DSchedule.Contracts.Models;

namespace DSchedule.Web.Controllers
{
    public class JobGraphController : Controller
    {
        JobMonitorBusinessComponent jobMonitorBusinessComponent = new JobMonitorBusinessComponent();
        JobMonitorModel model = new JobMonitorModel();
        
        // GET: JobGraph
        [HttpGet]
        public ActionResult GraphView(string SessionRunID)
        {
            if (Session["IsJobMonitor"]!=null)
            {
                return RedirectToAction("Index", "JobMonitor");
            }
            if (SessionRunID != null && SessionRunID != "undefined")
            {
                Session["SessionRunID"] = SessionRunID;
            }
            else
            {
                SessionRunID = Session["SessionRunID"].ToString();
            }
            model = jobMonitorBusinessComponent.GetJobSessionGraphDetails(SessionRunID);
            return View(model);
        }

        [HttpPost]
        public ActionResult ProcessAction(string UprocID, string SessionRunID, string UProcStatus)
        {
    
            JobMonitor jobMonitorModel = new JobMonitor();
            jobMonitorModel.UprocID = Convert.ToInt32(UprocID);
            jobMonitorModel.SessionRunID = SessionRunID;
            jobMonitorModel.UProcStatus = UProcStatus;
            switch (jobMonitorModel.UProcStatus)
            {
                case "Start":
                    jobMonitorModel.UProcStatus = "Start";
                    jobMonitorBusinessComponent.UpdateUProcStatusInMSMQ(jobMonitorModel);
                    jobMonitorModel.UProcStatus = "Started";
                    jobMonitorBusinessComponent.UpdateUProcStatusInSession(jobMonitorModel);
                    break;
                case "Re-Run":
                    jobMonitorModel.UProcStatus = "Re-Run";
                    jobMonitorBusinessComponent.UpdateUProcStatusInMSMQ(jobMonitorModel);
                    jobMonitorModel.UProcStatus = "Started";
                    jobMonitorBusinessComponent.UpdateUProcStatusInSession(jobMonitorModel);
                    break;
                case "Re-Schedule":
                    //jobMonitorModel.UProcStatus = "Re-Schedule";
                    jobMonitorBusinessComponent.UpdateUProcStatusInMSMQ(jobMonitorModel);
                    jobMonitorModel.UProcStatus = "Pending";
                    jobMonitorBusinessComponent.UpdateUProcStatusInSession(jobMonitorModel);
                    break;
                case "Hold":
                    jobMonitorModel.UProcStatus = "On Hold";
                    jobMonitorBusinessComponent.UpdateUProcStatusInSession(jobMonitorModel);
                    break;
                case "Abort":
                    jobMonitorModel.UProcStatus = "Aborted";
                    jobMonitorBusinessComponent.UpdateUProcStatusInSession(jobMonitorModel);
                    break;
                case "Force Completion":
                    jobMonitorModel.UProcStatus = "Completed";
                    jobMonitorBusinessComponent.UpdateUProcStatusInSession(jobMonitorModel);
                    break;
                case "Log":
                    // this status should remain the same... as we are just checking the logs.
                    break;
                case "Cancel":
                    jobMonitorModel.UProcStatus = "Cancelled";
                    jobMonitorBusinessComponent.UpdateUProcStatusInSession(jobMonitorModel);
                    break;
            }

            ModelState.Clear();
            model = jobMonitorBusinessComponent.GetJobSessionGraphDetails(SessionRunID);
            return View(model);
      
        }  
    }
  

    
}