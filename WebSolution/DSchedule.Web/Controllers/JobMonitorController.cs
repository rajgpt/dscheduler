﻿using DSchedule.BusinessComponent;
using DSchedule.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;

namespace DSchedule.Web.Controllers
{
    public class JobMonitorController : Controller
    {
        // GET: JobMonitoring       
        JobMonitorBusinessComponent jobMonitorBusinessComponent = new JobMonitorBusinessComponent();
        DScheduleBusinessComponent businessComponent = new DScheduleBusinessComponent();
        JobMonitorModel model = new JobMonitorModel();
        public ActionResult Index()
        {
            model = jobMonitorBusinessComponent.GetJobMonitorDetails();
            var jobActionTypes = businessComponent.GetReferenceDataNew("JobStatusTypeID");
            model.JobActionTypes = jobActionTypes;
            return View("JobMonitor", model);
        }

        // [OutputCache(NoStore = true, Location = OutputCacheLocation.Client, Duration = 15)] // every 3 sec
        public ActionResult GetJobMonitorDetails()
        {
            model = jobMonitorBusinessComponent.GetJobMonitorDetails();
            return PartialView("JobMonitorPartial", model);

        }

        public ActionResult GetUProcDetailsByID(string sessionrunid)
        {
            model = jobMonitorBusinessComponent.GetJobMonitorDetails();
            var results = (from m in model.JobMonitorList
                           where m.SessionRunID == sessionrunid
                           select new JobMonitor
                           {
                               UprocID = m.UprocID,
                               UprocName = m.UprocName,
                               SessionID = m.SessionID,
                               SessionName = m.SessionName,
                               UProcStatus = m.UProcStatus
                           }).FirstOrDefault();


            var jobActionTypes = businessComponent.GetReferenceData("JobActionTypeID");
            //var jobActiontypeList = businessComponent.GetRefTableValueDetails(jobActionTypes);

            var uprocStatus = results.UProcStatus;
            IEnumerable<SelectListItem> filteredList;
            switch (uprocStatus)
            {
                case "Pending":
                    {
                        filteredList = new List<SelectListItem> {
                            new SelectListItem { Text = "Start",Value = "Start"},
                            new SelectListItem { Text = "Re-Schedule",Value = "Re-Schedule" },
                            new SelectListItem { Text = "Hold",Value = "Hold" },
                            new SelectListItem { Text = "Log",Value = "Log" },
                            new SelectListItem { Text = "Cancel",Value = "Cancel" },
                            new SelectListItem { Text = "Session",Value = "Session" }
                        };
                        ViewBag.JobActionTypes = filteredList;
                        break;
                    }
                case "Failed":
                    {
                        filteredList = new List<SelectListItem> {
                            new SelectListItem { Text = "Re-Run",Value = "Re-Run"},
                            new SelectListItem { Text = "Session",Value = "Session" }
                            //new SelectListItem { Text = "Re-Schedule",Value = "Re-Schedule" },
                            //new SelectListItem { Text = "Hold",Value = "Hold" },
                            //new SelectListItem { Text = "Log",Value = "Log" },
                            //new SelectListItem { Text = "Cancel",Value = "Cancel" }
                        };
                        ViewBag.JobActionTypes = filteredList;
                        break;
                    }
                case "Started":
                    {
                        filteredList = new List<SelectListItem> {
                            new SelectListItem { Text = "Abort",Value = "Abort"},
                            new SelectListItem { Text = "Re-Schedule",Value = "Re-Schedule" },
                            new SelectListItem { Text = "Force Completion",Value = "Force Completion" },
                            new SelectListItem { Text = "Hold",Value = "Hold" },
                            new SelectListItem { Text = "Log",Value = "Log" },
                            new SelectListItem { Text = "Cancel",Value = "Cancel" },
                            new SelectListItem { Text = "Session",Value = "Session" }
                        };
                        ViewBag.JobActionTypes = filteredList;
                        break;
                    }

                case "Aborted":
                    {
                        filteredList = new List<SelectListItem> {
                            new SelectListItem { Text = "Re-Run",Value = "Re-Run" },
                            new SelectListItem { Text = "Re-Schedule",Value = "Re-Schedule" },
                            new SelectListItem { Text = "Log",Value = "Log" },
                            new SelectListItem { Text = "Session",Value = "Session" }
                        };
                        ViewBag.JobActionTypes = filteredList;
                        break;
                    }

                case "Completed":
                    {
                        filteredList = new List<SelectListItem> {
                            new SelectListItem { Text = "Re-Run",Value = "Re-Run" },
                            new SelectListItem { Text = "Re-Schedule",Value = "Re-Schedule" },
                            new SelectListItem { Text = "Log",Value = "Log" },
                            new SelectListItem { Text = "Session",Value = "Session" }
                        };
                        ViewBag.JobActionTypes = filteredList;
                        break;
                    }

                case "Cancelled":
                    {
                        filteredList = new List<SelectListItem> {
                            new SelectListItem { Text = "Start",Value = "Start"},
                            new SelectListItem { Text = "Re-Run",Value = "Re-Run" },
                            new SelectListItem { Text = "Re-Schedule",Value = "Re-Schedule" },
                            new SelectListItem { Text = "Log",Value = "Log" },
                            new SelectListItem { Text = "Session",Value = "Session" },
                            new SelectListItem { Text = "Session",Value = "Session" }
                        };
                        ViewBag.JobActionTypes = filteredList;
                        break;
                    }

                case "On Hold":
                    {
                        filteredList = new List<SelectListItem> {
                            new SelectListItem { Text = "Start",Value = "Start"},
                            new SelectListItem { Text = "Re-Run",Value = "Re-Run" },
                            new SelectListItem { Text = "Re-Schedule",Value = "Re-Schedule" },
                            new SelectListItem { Text = "Force Completion",Value = "Force Completion" },
                            new SelectListItem { Text = "Log",Value = "Log" },
                            new SelectListItem { Text = "Cancel",Value = "Cancel" },
                            new SelectListItem { Text = "Session",Value = "Session" }
                        };
                        ViewBag.JobActionTypes = filteredList;
                        break;
                    }
            }



            return PartialView("_UprocsDetailsPartialView", results);
        }

        [HttpPost]
        public ActionResult SearchUprocDetails(JobMonitorSearchCriteria SearchCriteria)
        {
            var jobMonitorList = jobMonitorBusinessComponent.JobMonitorSearch(SearchCriteria);
            var jobMonitorModel = new JobMonitorModel();
            jobMonitorModel.JobMonitorList = jobMonitorList;
            return PartialView("JobMonitorPartial", jobMonitorModel);
        }



        public ActionResult UpdateUProcStatusForSession(JobMonitor jobMonitorModel)
        {
            // jobMonitorBusinessComponent.UpdateUProcStatusInSession(jobMonitorModel);

            //Model contains the action information in the uprocstatus attribute.As per action information, uproc status needs to be updated.   
            //DScheduleBusinessComponent businessComponent = new DScheduleBusinessComponent();
            switch (jobMonitorModel.UProcStatus)
            {
                case "Start":
                    jobMonitorModel.UProcStatus = "Start";
                    jobMonitorBusinessComponent.UpdateUProcStatusInMSMQ(jobMonitorModel);
                    jobMonitorModel.UProcStatus = "Started";
                    jobMonitorBusinessComponent.UpdateUProcStatusInSession(jobMonitorModel);
                    break;
                case "Re-Run":
                    jobMonitorModel.UProcStatus = "Re-Run";
                    jobMonitorBusinessComponent.UpdateUProcStatusInMSMQ(jobMonitorModel);
                    jobMonitorModel.UProcStatus = "Started";
                    jobMonitorBusinessComponent.UpdateUProcStatusInSession(jobMonitorModel);
                    break;
                case "Re-Schedule":
                    //jobMonitorModel.UProcStatus = "Re-Schedule";
                    jobMonitorBusinessComponent.UpdateUProcStatusInMSMQ(jobMonitorModel);
                    jobMonitorModel.UProcStatus = "Pending";
                    jobMonitorBusinessComponent.UpdateUProcStatusInSession(jobMonitorModel);
                    break;
                case "Hold":
                    jobMonitorModel.UProcStatus = "On Hold";
                    jobMonitorBusinessComponent.UpdateUProcStatusInSession(jobMonitorModel);
                    break;
                case "Abort":
                    jobMonitorModel.UProcStatus = "Aborted";
                    jobMonitorBusinessComponent.UpdateUProcStatusInSession(jobMonitorModel);
                    break;
                case "Force Completion":
                    jobMonitorModel.UProcStatus = "Completed";
                    jobMonitorBusinessComponent.UpdateUProcStatusInSession(jobMonitorModel);
                    break;
                case "Log":
                    // this status should remain the same... as we are just checking the logs.
                    break;
                case "Cancel":
                    jobMonitorModel.UProcStatus = "Cancelled";
                    jobMonitorBusinessComponent.UpdateUProcStatusInSession(jobMonitorModel);
                    break;
                case "Session":
                    return RedirectToAction("GraphView", "JobGraph", new { SessionRunID = jobMonitorModel.SessionRunID.ToString() });
                    
                    
            }


            return RedirectToAction("Index", "JobMonitor");
        }


        public ActionResult ProcessAction(string UprocID, string sessionrunid, string UProcStatus)
        {

            JobMonitor jobMonitorModel = new JobMonitor();
            jobMonitorModel.SessionRunID = sessionrunid;
            jobMonitorModel.UprocID = Convert.ToInt32(UprocID);
            jobMonitorModel.UProcStatus = UProcStatus;
            Session["sessionrunid"] = sessionrunid;
            
            switch (jobMonitorModel.UProcStatus)
            {
                case "Start":
                    jobMonitorModel.UProcStatus = "Start";
                    jobMonitorBusinessComponent.UpdateUProcStatusInMSMQ(jobMonitorModel);
                    jobMonitorModel.UProcStatus = "Started";
                    jobMonitorBusinessComponent.UpdateUProcStatusInSession(jobMonitorModel);
                    Session["IsJobMonitor"] = "Yes";
                    break;
                case "Re-Run":
                    jobMonitorModel.UProcStatus = "Re-Run";
                    jobMonitorBusinessComponent.UpdateUProcStatusInMSMQ(jobMonitorModel);
                    jobMonitorModel.UProcStatus = "Started";
                    jobMonitorBusinessComponent.UpdateUProcStatusInSession(jobMonitorModel);
                    Session["IsJobMonitor"] = "Yes";
                    break;
                case "Re-Schedule":
                    //jobMonitorModel.UProcStatus = "Re-Schedule";
                    jobMonitorBusinessComponent.UpdateUProcStatusInMSMQ(jobMonitorModel);
                    jobMonitorModel.UProcStatus = "Pending";
                    jobMonitorBusinessComponent.UpdateUProcStatusInSession(jobMonitorModel);
                    Session["IsJobMonitor"] = "Yes";
                    break;
                case "Hold":
                    jobMonitorModel.UProcStatus = "On Hold";
                    jobMonitorBusinessComponent.UpdateUProcStatusInSession(jobMonitorModel);
                    Session["IsJobMonitor"] = "Yes";
                    break;
                case "Abort":
                    jobMonitorModel.UProcStatus = "Aborted";
                    jobMonitorBusinessComponent.UpdateUProcStatusInSession(jobMonitorModel);
                    Session["IsJobMonitor"] = "Yes";
                    break;
                case "Force Completion":
                    jobMonitorModel.UProcStatus = "Completed";
                    jobMonitorBusinessComponent.UpdateUProcStatusInSession(jobMonitorModel);
                    Session["IsJobMonitor"] = "Yes";
                    break;
                case "Log":
                    // this status should remain the same... as we are just checking the logs.
                    Session["IsJobMonitor"] = "Yes";
                    break;
                case "Cancel":
                    jobMonitorModel.UProcStatus = "Cancelled";
                    jobMonitorBusinessComponent.UpdateUProcStatusInSession(jobMonitorModel);
                    Session["IsJobMonitor"] = "Yes";
                    break;
                case "Session":
                    Session["IsJobMonitor"] = null;
                    return Json(new { SessionRunId = sessionrunid}, JsonRequestBehavior.AllowGet);
                    
            }

            return RedirectToAction("Index", "JobMonitor");

        }

        public ActionResult GetUProcDetailsByUprocSessionID(string SessionRunID)
        {
            model = jobMonitorBusinessComponent.GetJobMonitorDetails();
            var results = (from m in model.JobMonitorList
                           where m.SessionRunID == SessionRunID
                           select new JobMonitor
                           {
                               UprocID = m.UprocID,
                               UprocName = m.UprocName,
                               SessionID = m.SessionID,
                               SessionName = m.SessionName,
                               UProcStatus = m.UProcStatus
                           }).FirstOrDefault();


            var jobActionTypes = businessComponent.GetReferenceData("JobActionTypeID");
            //var jobActiontypeList = businessComponent.GetRefTableValueDetails(jobActionTypes);

            var uprocStatus = results.UProcStatus;
            List<SelectListItem> filteredList ;
            List<SelectListItem> optionObj = new List<SelectListItem>();
            switch (uprocStatus)
            {
                case "Pending":
                    {
                        filteredList = new List<SelectListItem> {
                            new SelectListItem { Text = "'Start'",Value = "Start"},
                            new SelectListItem { Text = "'Re-Schedule'",Value = "Re-Schedule" },
                            new SelectListItem { Text = "'Hold'",Value = "Hold" },
                            new SelectListItem { Text = "'Log'",Value = "Log" },
                            new SelectListItem { Text = "'Cancel'",Value = "Cancel" },
                            new SelectListItem { Text = "'Session'",Value = "Session" }
                        };
                        optionObj = filteredList;
                        break;
                    }
                case "Failed":
                    {
                        filteredList = new List<SelectListItem> {
                            new SelectListItem { Text = "'Re-Run'",Value = "Re-Run"},
                            new SelectListItem { Text = "'Session'",Value = "Session" }
                            //new SelectListItem { Text = "Re-Schedule",Value = "Re-Schedule" },
                            //new SelectListItem { Text = "Hold",Value = "Hold" },
                            //new SelectListItem { Text = "Log",Value = "Log" },
                            //new SelectListItem { Text = "Cancel",Value = "Cancel" }
                        };
                        optionObj = filteredList;
                        break;
                    }
                case "Started":
                    {
                        filteredList = new List<SelectListItem> {
                            new SelectListItem { Text = "'Abort'",Value = "Abort"},
                            new SelectListItem { Text = "'Re-Schedule'",Value = "Re-Schedule" },
                            new SelectListItem { Text = "'Force Completion'",Value = "Force Completion" },
                            new SelectListItem { Text = "'Hold'",Value = "Hold" },
                            new SelectListItem { Text = "'Log'",Value = "Log" },
                            new SelectListItem { Text = "'Cancel'",Value = "Cancel" },
                            new SelectListItem { Text = "'Session'",Value = "Session" }
                        };
                        optionObj = filteredList;
                        break;
                    }

                case "Aborted":
                    {
                        filteredList = new List<SelectListItem> {
                            new SelectListItem { Text = "'Re-Run'",Value = "Re-Run" },
                            new SelectListItem { Text = "'Re-Schedule'",Value = "Re-Schedule" },
                            new SelectListItem { Text = "'Log'",Value = "Log" },
                            new SelectListItem { Text = "'Session'",Value = "Session" }
                        };
                        optionObj = filteredList;
                        break;
                    }

                case "Completed":
                    {
                        filteredList = new List<SelectListItem> {
                            new SelectListItem { Text = "'Re-Run'",Value = "Re-Run" },
                            new SelectListItem { Text = "'Re-Schedule'",Value = "Re-Schedule" },
                            new SelectListItem { Text = "'Log'",Value = "Log" },
                            new SelectListItem { Text = "'Session'",Value = "Session" }
                        };
                        optionObj = filteredList;
                        break;
                    }

                case "Cancelled":
                    {
                        filteredList = new List<SelectListItem> {
                            new SelectListItem { Text = "'Start'",Value = "Start"},
                            new SelectListItem { Text = "'Re-Run'",Value = "Re-Run" },
                            new SelectListItem { Text = "'Re-Schedule'",Value = "Re-Schedule" },
                            new SelectListItem { Text = "'Log'",Value = "Log" },
                            new SelectListItem { Text = "'Session'",Value = "Session" },
                            new SelectListItem { Text = "Session",Value = "Session" }
                        };
                        optionObj = filteredList;
                        break;
                    }

                case "On Hold":
                    {
                        filteredList = new List<SelectListItem> {
                            new SelectListItem { Text = "'Start'",Value = "Start"},
                            new SelectListItem { Text = "'Re-Run'",Value = "Re-Run" },
                            new SelectListItem { Text = "'Re-Schedule'",Value = "Re-Schedule" },
                            new SelectListItem { Text = "'Force Completion'",Value = "Force Completion" },
                            new SelectListItem { Text = "'Log'",Value = "Log" },
                            new SelectListItem { Text = "'Cancel'",Value = "Cancel" },
                            new SelectListItem { Text = "'Session'",Value = "Session" }
                        };
                        optionObj = filteredList;
                        break;
                    }
            }



            return Json(new { optionObj = optionObj }, JsonRequestBehavior.AllowGet);
        }

    }
}