﻿using DSchedule.BusinessComponent;
using DSchedule.Contracts;
using DSchedule.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Environment = DSchedule.Contracts.Models.Environment;

namespace DSchedule.Web.Controllers
{
    [Authorize]
    public class JobDesignController : Controller
    {
        DScheduleBusinessComponent businessComponent = new DScheduleBusinessComponent();

        #region Nodes
        public ActionResult NodesView()
        {
            try
            {
                NodesModel nm = businessComponent.GetNodes();
                return View("NodesView", nm);
            }
            catch (Exception ex)
            {
                int ErrorId = ErrorLoggingBusinessComponent.SendExcepToDB(ex);
                return RedirectToAction("Error", "Home");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult NodesView(NodesModel model)
        {
            try
            {
                Node node = new Node();
                var success = true;
                if (model != null)
                {
                    bool nodeExists = businessComponent.GetNodeList().Any(x => x.NodeName == model.NodeName);
                    if (nodeExists)
                    {
                        model = businessComponent.GetNodes();
                        ModelState.AddModelError("NodeName", "Node already Exists !");
                        return View("NodesView", model);
                    }

                    if (ModelState.IsValid)
                    {
                        node.NodeID = model.NodeID;
                        node.NodeName = model.NodeName;
                        node.NodePath = model.Path;

                        success = businessComponent.SaveNode(node);
                    }
                    else
                    {
                        model = businessComponent.GetNodes();
                        return View(model);
                    }
                }

                return RedirectToAction("NodesView");
            }
            catch (Exception ex)
            {
                int ErrorId = ErrorLoggingBusinessComponent.SendExcepToDB(ex);
                return RedirectToAction("Error", "Home");
            }
        }
        #endregion

        #region Environment
        public ActionResult EnvironmentView()
        {
            try
            {
                EnvironmentModel model = new EnvironmentModel();
                model = businessComponent.GetEnvironments();
                return View("EnvironmentView", model);
            }
            catch (Exception ex)
            {
                int ErrorId = ErrorLoggingBusinessComponent.SendExcepToDB(ex);
                return RedirectToAction("Error", "Home");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EnvironmentView(EnvironmentModel transaction)
        {
            try
            {
                bool nodeExists = transaction.EnvironmentList.Any(x => x.EnvironmentName == transaction.EnvironmentName);
                if (nodeExists)
                {
                    ModelState.AddModelError("EnvironmentName", "Environment already Exists !");
                }

                if (ModelState.IsValid)
                {
                    Environment environment = new Environment();

                    if (environment != null)
                    {
                        environment.EnvironmentID = transaction.EnvironmentID;
                        environment.EnvironmentName = transaction.EnvironmentName;
                        environment.NodeID = transaction.NodeID;
                        environment.EnvironmentPath = transaction.EnvironmentPath;

                        var success = businessComponent.SaveEnvironment(environment);
                    }

                    return RedirectToAction("EnvironmentView");
                }
                transaction.NodesList = businessComponent.GetNodeList();
                return View(transaction);
            }
            catch (Exception ex)
            {
                int ErrorId = ErrorLoggingBusinessComponent.SendExcepToDB(ex);
                return RedirectToAction("Error", "Home");
            }
        }
        #endregion

        #region Uprocs
        public ActionResult UprocsView()
        {
            try
            {
                UprocsModel uprocs = businessComponent.GetUprocs();
                uprocs = SetUprocsReferenceData(uprocs);
                ViewBag.JobRef = uprocs.JobTypeReferenceData;
                ViewBag.EnvironmentRef = uprocs.EnvironmentReferenceData;
                ViewBag.AccountRef = uprocs.AccountTypeReferenceData;
                ViewBag.IsActive = GetIsActiveValues();
                if (uprocs.UprocsList.Any())
                {
                    foreach (var item in uprocs.UprocsList)
                    {
                        item.JobTypeName = uprocs.JobTypeReferenceData.Where(x => x.RefId == item.JobTypeID).FirstOrDefault().RefValue;
                        item.AccountName = uprocs.AccountTypeReferenceData.Where(x => x.RefId == item.AccountTypeID).FirstOrDefault().RefValue;
                        item.EnvironmentName = uprocs.EnvironmentReferenceData.Where(x => x.RefId == item.EnvironmentID).FirstOrDefault().RefValue;
                    }
                }
                return View("UprocsView", uprocs);
            }
            catch (Exception ex)
            {
                int ErrorId = ErrorLoggingBusinessComponent.SendExcepToDB(ex);
                return RedirectToAction("Error", "Home");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UprocsView(UprocsModel model)
        {
            try
            {
                UprocsModel uprocs = new UprocsModel();
                uprocs = SetUprocsReferenceData(uprocs);
                ViewBag.JobRef = uprocs.JobTypeReferenceData;
                ViewBag.EnvironmentRef = uprocs.EnvironmentReferenceData;
                ViewBag.AccountRef = uprocs.AccountTypeReferenceData;
                ViewBag.IsActive = GetIsActiveValues();
                model.EnvironmentName = uprocs.EnvironmentReferenceData.FirstOrDefault(x => x.RefId == model.EnvironmentID).RefValue;
                ModelState.Remove("UprocsList");
                ModelState.Remove("JobTypeReferenceData");
                ModelState.Remove("EnvironmentReferenceData");
                ModelState.Remove("AccountTypeReferenceData");
                if (ModelState.IsValid)
                {
                    uprocs.UprocID = model.UprocID;
                    uprocs.EnvironmentID = model.EnvironmentID;
                    uprocs.JobTypeID = model.JobTypeID;
                    uprocs.AccountTypeID = model.AccountTypeID;
                    uprocs.UprocName = model.UprocName;
                    uprocs.EnvironmentName = model.EnvironmentName;
                    uprocs.FolderPath = model.FolderPath;
                    uprocs.Command = model.Command;
                    uprocs.IsActive = model.IsActive;
                    businessComponent.SaveUprocDetails(uprocs);
                    return RedirectToAction("UprocsView");
                }
                ViewBag.JobRef = uprocs.JobTypeReferenceData;
                ViewBag.EnvironmentRef = uprocs.EnvironmentReferenceData;
                ViewBag.AccountRef = uprocs.AccountTypeReferenceData;
                var obj = businessComponent.GetUprocs();
                if (obj.UprocsList.Any())
                {
                    model.UprocsList = new List<Uprocs>();
                    foreach (var item in obj.UprocsList)
                    {
                        model.UprocsList.Add(item);
                    }
                }
                return View(model);
            }
            catch (Exception ex)
            {
                int ErrorId = ErrorLoggingBusinessComponent.SendExcepToDB(ex);
                return RedirectToAction("Error", "Home");
            }
        }
        #endregion

        #region Session
        public ActionResult SessionsView()
        {
            try
            {
                SessionsModel sessionsModel = businessComponent.GetSessions();
                sessionsModel = SetSessionsReferenceData(sessionsModel);
                ViewBag.UprocsRef = sessionsModel.UprocsReferenceData;
                ViewBag.EnvironmentRef = sessionsModel.EnvironmentReferenceData;
                ViewBag.UserRef = sessionsModel.UserReferenceData;
                ViewBag.SessionsRef = sessionsModel.SessionsReferenceData;
                ViewBag.DependentUprocs = sessionsModel.DependentUprocsReferenceData;
                if (sessionsModel.SessionsList.Any())
                {
                    foreach (var item in sessionsModel.SessionsList)
                    {
                        item.Uproc = sessionsModel.UprocsReferenceData.Where(x => x.RefId == item.UprocId).FirstOrDefault().RefValue;
                        item.User = sessionsModel.UserReferenceData.Where(x => x.RefId == item.AccountTypeID).FirstOrDefault().RefValue;
                        item.Environment = sessionsModel.EnvironmentReferenceData.Where(x => x.RefId == item.EnvironmentID).FirstOrDefault().RefValue;
                    }
                }
                return View("SessionsView", sessionsModel);
            }
            catch (Exception ex)
            {
                int ErrorId = ErrorLoggingBusinessComponent.SendExcepToDB(ex);
                return RedirectToAction("Error", "Home");
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SessionsView(SessionsModel model, string command)
        {
            try
            {
                SessionsModel request = new SessionsModel();
                request = SetSessionsReferenceData(request);
                request.command = command;
                ViewBag.UprocsRef = request.UprocsReferenceData;
                ViewBag.EnvironmentRef = request.EnvironmentReferenceData;
                ViewBag.UserRef = request.UserReferenceData;
                ViewBag.SessionsRef = request.SessionsReferenceData;
                ViewBag.DependentUprocs = request.DependentUprocsReferenceData;
                model.EnvironmentName = request.EnvironmentReferenceData.FirstOrDefault(x => x.RefId == model.EnvironmentID).RefValue;
                ModelState.Remove("SessionsList");
                ModelState.Remove("UprocsReferenceData");
                ModelState.Remove("EnvironmentReferenceData");
                ModelState.Remove("UserReferenceData");
                ModelState.Remove("SessionsReferenceData");
                ModelState.Remove("DependentUprocsReferenceData");
                if (command == "Save")
                {
                    if (ModelState.IsValid)
                    {
                        request.SessionId = model.SessionId;
                        request.SessionName = model.SessionName;
                        request.EnvironmentID = model.EnvironmentID;
                        request.EnvironmentName = model.EnvironmentName;
                        request.AccountTypeID = model.AccountTypeID;
                        request.UprocID = model.UprocID;
                        businessComponent.SaveSessions(request);
                        return RedirectToAction("SessionsView");
                    }
                }
                else if (command == "Add")
                {
                    request.DependentSessionId = model.DependentSessionId;
                    request.DependentUprocId = model.DependentUprocId;
                    businessComponent.SaveSessions(request);
                    return RedirectToAction("SessionsView");
                }
                SessionsModel sm = businessComponent.GetSessions();
                model.SessionsList = sm.SessionsList;
                return View(model);
            }
            catch (Exception ex)
            {
                int ErrorId = ErrorLoggingBusinessComponent.SendExcepToDB(ex);
                return RedirectToAction("Error", "Home");
            }
        }
        #endregion

        #region JobRules
        public ActionResult JobRulesView()
        {
            try
            {
                return View(GetPageData());
            }
            catch (Exception ex)
            {
                int ErrorId = ErrorLoggingBusinessComponent.SendExcepToDB(ex);
                return RedirectToAction("Error", "Home");
            }
        }

        public JobRulesModel GetPageData()
        {
            JobRulesModel jobRules = new JobRulesModel();
            jobRules = businessComponent.GetJobRules();
            List<ReferenceData> listRefData = businessComponent.GetSessionsReferenceData();
            jobRules.UprocsReferenceData = listRefData.Where(x => x.RefName.Equals("SessionsID")).ToList();
            ViewBag.UprocSessionRef = jobRules.UprocsReferenceData;
            ViewBag.ListOutageDetails = new List<SelectListItem>();
            return jobRules;

        }
        //[HttpPost]
        //public ActionResult JobRulesView(JobRulesModel model)
        //{
        //    JobRulesModel viewobj = new JobRulesModel();
        //    viewobj = businessComponent.GetJobRules();
        //    List<JobRules> listJobRules = new List<JobRules>();
        //    List<ReferenceData> listRefData = businessComponent.GetSessionsReferenceData();
        //    viewobj.UprocsReferenceData = listRefData.Where(x => x.RefName.Equals("UprocsTypeID") || x.RefName.Equals("SessionsID")).ToList();
        //    ViewBag.UprocSessionRef = viewobj.UprocsReferenceData;
        //    ViewBag.ListOutageDetails = new List<SelectListItem>();
        //    viewobj.JobStartDate = model.JobStartDate;
        //    viewobj.ActivityTypeID = model.ActivityTypeID;
        //    viewobj.UprocOrSessionId = model.UprocOrSessionId;
        //    viewobj.JobRuleId = model.JobRuleId;
        //    ModelState.Remove("JobRulesList");
        //    ModelState.Remove("Outage");
        //    ModelState.Remove("UprocsReferenceData");
        //    ModelState.Remove("SessionsReferenceData");
        //    if (ModelState.IsValid)
        //    {
        //        businessComponent.SaveJobRule(viewobj);
        //        return RedirectToAction("JobRulesView");
        //    }
        //    else
        //    {
        //        return View(viewobj);
        //    }
        //}
        [HttpPost]
        public ActionResult JobRulesView(FormCollection fc, JobRulesModel model)
        {
            JobRulesModel viewobj = new JobRulesModel();
            List<Outage> listOutage = new List<Outage>();
            string[] indexes = fc.GetValues("Outage.index");
            if (indexes != null && indexes.Length > 0)
            {
                var cultureSource = new CultureInfo("en-US", false);
                var cultureDest = new CultureInfo("de-DE", false);
                foreach (var index in indexes)
                {
                   listOutage.Add(new Outage()
                    {
                        //TODO
                        OutageId = Convert.ToInt32(String.Format("Outage[" + index + "].OutageId").FirstOrDefault()),
                        OutageName = fc.GetValues("Outage[" + index + "].OutageName").FirstOrDefault(),
                        OutageStartDate = fc.GetValues("Outage[" + index + "].OutageStartDate").FirstOrDefault().ToString(),
                        OutageStartTime = DateTime.Parse(fc.GetValues("Outage[" + index + "].OutageStartTime").FirstOrDefault(), cultureSource).ToString("t", cultureDest),
                        OutageEndTime = DateTime.Parse(fc.GetValues("Outage[" + index + "].OutageEndTime").FirstOrDefault(), cultureSource).ToString("t", cultureDest)
                    });
                }
            }
            viewobj.Outage = listOutage;
            List<JobRules> listJobRules = new List<JobRules>();
            List<ReferenceData> listRefData = businessComponent.GetSessionsReferenceData();
            viewobj.UprocsReferenceData = listRefData.Where(x => x.RefName.Equals("UprocsTypeID") || x.RefName.Equals("SessionsID")).ToList();
            ViewBag.UprocSessionRef = viewobj.UprocsReferenceData;
            ViewBag.ListOutageDetails = new List<SelectListItem>();
            viewobj.JobStartDate = model.JobStartDate;
            viewobj.ActivityTypeID = model.ActivityTypeID;
            viewobj.UprocOrSessionId = model.UprocOrSessionId;
            viewobj.IsRecurrence = model.IsRecurrence;
            viewobj.JobRuleId = model.JobRuleId;
            viewobj.RecurrencePattern = !string.IsNullOrEmpty(model.RecurrencePattern) ? model.RecurrencePattern : null;
            viewobj.RecurrenceStartDate = model.RecurrenceStartDate != null ? model.RecurrenceStartDate : null;
            viewobj.RecurrenceEndDate = model.RecurrenceEndDate != null ? model.RecurrenceEndDate : null;
            viewobj.IsMonday = model.IsMonday;
            viewobj.IsTuesday = model.IsTuesday;
            viewobj.IsWednesday = model.IsWednesday;
            viewobj.IsThursday = model.IsThursday;
            viewobj.IsFriday = model.IsFriday;
            viewobj.IsSaturday = model.IsSaturday;
            viewobj.IsSunday = model.IsSunday;
            viewobj.RMDay = model.RMDay;
            viewobj.RYDay = model.RYDay;
            ModelState.Remove("JobRulesList");
            ModelState.Remove("Outage");
            ModelState.Remove("UprocsReferenceData");
            ModelState.Remove("SessionsReferenceData");
            if (model.IsRecurrence && businessComponent.checkJobRule(viewobj) && viewobj.JobRuleId ==0) {
                ViewBag.Message = "Record already exists";
                return View(GetPageData());
            }
            //if (ModelState.IsValid)
            //{
                
                businessComponent.SaveJobRule(viewobj);
                return RedirectToAction("JobRulesView");
            //}
            //else
            //{
            //    return View(viewobj);
            //}
        }

        [HttpPost]
        public ActionResult OutageView(int jobRuleID)
        {
            List<Outage> listOutage = businessComponent.getOutageDetailsbyJobRuleID(jobRuleID);
            return Json(listOutage, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public PartialViewResult Add(string OutageName, string OutageDate, string OutageFrom, string OutageTo)
        {
            Outage o = new Outage { OutageName = OutageName, OutageStartDate = OutageDate, OutageStartTime= OutageFrom, OutageEndTime=OutageTo};
            return PartialView("_OutageView", o);
        }
        #endregion
        [AllowAnonymous]
        public ActionResult GetSessionOrUproc(int id)
        {
            List<ReferenceData> listRefData = businessComponent.GetSessionsReferenceData();
            if (id == 9)
            {
                listRefData = listRefData.Where(x => x.RefName.Equals("UprocsTypeID")).ToList();
            }
            else if (id == 10)
            {
                listRefData = listRefData.Where(x => x.RefName.Equals("SessionsID")).ToList();
            }
            return Json(listRefData, JsonRequestBehavior.AllowGet);
        }
        #region Common
        public UprocsModel SetUprocsReferenceData(UprocsModel model)
        {
            List<ReferenceData> listRefData = businessComponent.GetUprocsReferenceData();
            model.JobTypeReferenceData = listRefData.Where(x => x.RefName.Equals("JobTypeID")).ToList();
            model.EnvironmentReferenceData = listRefData.Where(x => x.RefName.Equals("EnvironmentTypeID")).ToList();
            model.AccountTypeReferenceData = listRefData.Where(x => x.RefName.Equals("AccountTypeID")).ToList();
            return model;
        }
        public SessionsModel SetSessionsReferenceData(SessionsModel model)
        {
            List<ReferenceData> listRefData = businessComponent.GetSessionsReferenceData();
            model.UprocsReferenceData = listRefData.Where(x => x.RefName.Equals("UprocsTypeID")).ToList();
            model.EnvironmentReferenceData = listRefData.Where(x => x.RefName.Equals("EnvironmentTypeID")).ToList();
            model.UserReferenceData = listRefData.Where(x => x.RefName.Equals("AccountTypeID")).ToList();
            model.SessionsReferenceData = listRefData.Where(x => x.RefName.Equals("SessionsID")).ToList();
            model.DependentUprocsReferenceData = model.UprocsReferenceData;//listRefData.Where(x => x.RefName.Equals("DependentUprocsID")).ToList();
            return model;
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Cancel()
        {
            return View();
        }
        private dynamic GetIsActiveValues()
        {
            List<SelectListItem> SelectListItemlist = new List<SelectListItem>();
            SelectListItem IsActiveYes = new SelectListItem()
            {
                Text = "Yes",
                Value = "1"
            };
            SelectListItem IsActiveNo = new SelectListItem()
            {
                Text = "No",
                Value = "0"
            };
            SelectListItemlist.Add(IsActiveYes);
            SelectListItemlist.Add(IsActiveNo);

            return new SelectList(SelectListItemlist, "Value", "Text");
        }
        #endregion
    }
}