﻿using ClosedXML.Excel;
using DSchedule.BusinessComponent;
using DSchedule.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;
using ReportData = DSchedule.Contracts.Models.ReportData;
using ReportsModel = DSchedule.Contracts.Models.ReportsModel;

namespace DSchedule.Web.Controllers
{
    public class ReportsController : Controller
    {
        DScheduleBusinessComponent businessComponent = new DScheduleBusinessComponent();
        #region Rajat
       
        private dynamic GetdropdownTemplateValues()
        {
            List<SelectListItem> SelectListItemlist = new List<SelectListItem>();
            SelectListItem item = new SelectListItem()
            {
                Text = "TPT_FIN_Monitor",
                Value = "TPT_FIN_Monitor"
            };

            SelectListItemlist.Add(item);
            return new SelectList(SelectListItemlist, "Value", "Text");
        }
        private dynamic GetEnvironmentValues()
        {
            List<SelectListItem> SelectListItemlist = new List<SelectListItem>();
            SelectListItem item = new SelectListItem()
            {
                Text = "UAT",
                Value = "UAT"
            };

            SelectListItemlist.Add(item);
            return new SelectList(SelectListItemlist, "Value", "Text");
        }
        private dynamic GetNodeValues()
        {
            List<SelectListItem> SelectListItemlist = new List<SelectListItem>();
            SelectListItem item = new SelectListItem()
            {
                Text = "WIN290",
                Value = "WIN290"
            };

            SelectListItemlist.Add(item);
            return new SelectList(SelectListItemlist, "Value", "Text");
        }
        #endregion Rajat

        public ActionResult ReportsView()
        {
            ReportsModel rm = new ReportsModel();
            rm.ReportsList = new List<ReportsData>();
            List<ReferenceData> listRefData = businessComponent.GetReportsReferenceData();
            ViewBag.Nodes = listRefData.Where(x => x.RefName == "Nodes").OrderBy(x => x.RefId);
            ViewBag.Sessions = listRefData.Where(x => x.RefName == "Sessions").OrderBy(x => x.RefId); ;
            ViewBag.Uprocs = listRefData.Where(x => x.RefName == "Uprocs").OrderBy(x => x.RefId); ;
            ViewBag.Environments = listRefData.Where(x => x.RefName == "Environments").OrderBy(x => x.RefId);
            ViewBag.Templates = listRefData.Where(x => x.RefName == "Templates").OrderBy(x => x.RefId);
            ViewBag.Status = listRefData.Where(x => x.RefName == "Status").OrderBy(x => x.RefId);

            return View(rm);
        }

        [HttpPost]
        public ActionResult ReportsView(ReportsModel model, string btnGenerate, string btnExport)
        {
            List<ReferenceData> listRefData = businessComponent.GetReportsReferenceData();
            ViewBag.Nodes = listRefData.Where(x => x.RefName == "Nodes").OrderBy(x => x.RefId);
            ViewBag.Sessions = listRefData.Where(x => x.RefName == "Sessions").OrderBy(x => x.RefId); ;
            ViewBag.Uprocs = listRefData.Where(x => x.RefName == "Uprocs").OrderBy(x => x.RefId); ;
            ViewBag.Environments = listRefData.Where(x => x.RefName == "Environments").OrderBy(x => x.RefId);
            ViewBag.Templates = listRefData.Where(x => x.RefName == "Templates").OrderBy(x => x.RefId);
            ViewBag.Status = listRefData.Where(x => x.RefName == "Status").OrderBy(x => x.RefId);
            ReportsModel obj = new ReportsModel();
            obj.ReportsList = businessComponent.GenerateReportsData(model);

            if (btnExport != null)
            {
                return ExportToExcel(obj.ReportsList);
            }
            return View("ReportsView", obj);
        }

        public FileContentResult ExportToExcel(List<ReportsData> list)
        {
            string ReportsPath = Convert.ToString(ConfigurationManager.AppSettings["ReportsPath"]);
            XLWorkbook wb = new XLWorkbook();
            string sheetName = "Dscheduler_Report";
            var ws = wb.Worksheets.Add(sheetName);
            ws.Cell(1, 1).InsertTable(list);
            byte[] xlsInBytes;
            using (MemoryStream memoryStream = new MemoryStream())
            {
                wb.SaveAs(memoryStream);
                xlsInBytes = memoryStream.ToArray();
                return File(xlsInBytes, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", sheetName + DateTime.Now.ToString("yyyy-dd-M--HH-mm-ss")+".xlsx");
                //System.IO.File.WriteAllBytes(ReportsPath + sheetName + Guid.NewGuid().ToString() + ".xlsx", memoryStream.ToArray());
            }
        }
    }
}