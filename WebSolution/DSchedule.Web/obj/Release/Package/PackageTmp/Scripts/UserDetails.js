﻿$(document).ready(function () {
    $(function () {
        $('#firstname, #lastname').keyup(function () {
                if (this.value.match(/[^a-zA-Z]/g)) {
                    this.value = this.value.replace(/[^a-zA-Z]/g, '');
                }
        });
        $(".edit-mode").hide();
        
        pageGrids.ordersGrid.onRowSelect(function (e) {
            //$('#EnvironmentID').val(e.row.EnvironmentID);
            //$('#EnvironmentName').val(e.row.EnvironmentName);
            //$('#EnvironmentPath').val(e.row.EnvironmentPath);
            //$('#NodeID').val(e.row.NodeID);
            $("#userId").val(e.row.LoginID);
            $("#username").val(e.row.UserName);
            $("#firstname").val(e.row.FirstName);
            $("#lastname").val(e.row.LastName);
            $("#email").val(e.row.Email);
            $("#accounttype").val(e.row.AccountTypeId);
            $("#organization").val(e.row.Organization);
            $('#isactive').val(e.row.IsActiveID);
            $(".edit-mode").show();
            $(".create-mode").hide();

        });
        $('#btnCancel').click(function () {
            $("#hdnUprocId").val('');
            $("#username").val('');
            $("#firstname").val('');
            $("#lastname").val('');
            $("#email").val('');
            $("#accounttype").val('');
            $("#organization").val('');
            $("#isactive").val('');
        });
    });
});

function OnNewClick() {
    $(".edit-mode").hide();
    $(".create-mode").show();
    $('#btnCancel').click();
    $('#lastname').val(null);
    $('#email').val(null);
    $("#accounttype").val(null);
}