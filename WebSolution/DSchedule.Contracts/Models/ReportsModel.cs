﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace DSchedule.Contracts.Models
{
    public class ReportsModel
    {
        public ReportsModel()
        {
            ReportsList = new List<ReportsData>();
        }
        [Display(Name = "Report ID")]
        public int ReportID { get; set; }

        [Display(Name = "Template ID")]
        public string TemplateID { get; set; }

        [Display(Name = "Template Name")]
        public string TemplateName { get; set; }

        [Display(Name = "Environment")]
        public string Environment { get; set; }

        public int EnvironmentId { get; set; }

        [Display(Name = "Node")]
        public string Node { get; set; }

        public int NodeId { get; set; }

        public string Uproc { get; set; }

        public int UprocId { get; set; }

        public string Session { get; set; }

        public int SessionId { get; set; }

        [Display(Name = "Created Date/Time")]
        public string CreatedDate { get; set; }

        [Display(Name = "Created By")]
        public string CreatedBy { get; set; }

        public int StatusId { get; set; }

        public string Status { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public List<ReportsData> ReportsList { get; set; }
    }
}
