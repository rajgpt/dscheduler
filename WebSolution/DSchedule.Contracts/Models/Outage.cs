﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSchedule.Contracts.Models
{
    public class Outage
    {
        public int OutageId { get; set; }
        public string OutageName { get; set; }
        public DateTime? CreatedDateTime { get; set; }
        public DateTime? UpdatedDateTime { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public int JobRuleId { get; set; }
        public DateTime? OutageEndDate { get; set; }
        public string OutageStartTime { get; set; }
        public string OutageEndTime { get; set; }
        public string OutageStartDate { get; set; }
    }
}
