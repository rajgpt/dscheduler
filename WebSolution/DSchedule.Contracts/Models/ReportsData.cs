﻿using System;

namespace DSchedule.Contracts.Models
{
    public class ReportsData
    {
        [ClosedXML.Attributes.XLColumn(Ignore = true)]
        public int JobId { get; set; }

        [ClosedXML.Attributes.XLColumn(Ignore = true)]
        public int NodeId { get; set; }

        [ClosedXML.Attributes.XLColumn(Header = "Node Name")]
        public string NodeName { get; set; }

        [ClosedXML.Attributes.XLColumn(Ignore = true)]
        public int EnvironmentId { get; set; }

        [ClosedXML.Attributes.XLColumn(Header = "Environment Name")]
        public string  EnvironmentName { get; set; }

        [ClosedXML.Attributes.XLColumn(Ignore = true)]
        public int SessionId { get; set; }

        [ClosedXML.Attributes.XLColumn(Header = "Session Name")]
        public string SessionName { get; set; }

        [ClosedXML.Attributes.XLColumn(Ignore = true)]
        public int UprocId { get; set; }

        [ClosedXML.Attributes.XLColumn(Header = "Uproc Name")]
        public string UprocName { get; set; }

        [ClosedXML.Attributes.XLColumn(Header = "Start Time")]
        public DateTime? StartTime { get; set; }

        [ClosedXML.Attributes.XLColumn(Header = "End Time")]
        public DateTime? EndTime { get; set; }

        [ClosedXML.Attributes.XLColumn(Header = "Status")]
        public string Status { get; set; }

        [ClosedXML.Attributes.XLColumn(Header = "Elapsed Time")]
        public TimeSpan ElapsedTime { get; set; }

    }
}
