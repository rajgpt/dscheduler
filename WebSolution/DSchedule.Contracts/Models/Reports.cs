﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace DSchedule.Contracts.Models
{
    public class ReportData
    {
        [Display(Name = "Report ID")]
        public int ReportID { get; set; }
        [Display(Name = "Template ID")]
        public string TemplateID { get; set; }
        [Display(Name = "Template Name")]
        public string TemplateName { get; set; }
        [Display(Name = "Environment")]
        public string Environment { get; set; }
        [Display(Name = "Node")]
        public string Node { get; set; }
        [Display(Name = "Created Date/Time")]
        public DateTime CreatedDate { get; set; }
        [Display(Name = "Created By")]
        public string CreatedBy { get; set; }
    }
}
