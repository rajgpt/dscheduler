﻿using DSchedule.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DSchedule.Contracts.Models
{
    public class NodesModel
    {
        public int NodeID { get; set; }

        [Required]
        [Display(Name = "Node Name")]
        [MaxLength(25, ErrorMessage = "Node name should be less than or equal to 25 characters")]
        public string NodeName { get; set; }

        [Required]
        [Display(Name = "Path")]
        public string Path { get; set; }

        public DateTime AddedDate { get; set; }

        public List<Node> NodesList { get; set; }
    }
}